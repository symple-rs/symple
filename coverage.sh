export RUSTC_BOOSTRAP=1
export CARGO_INCREMENTAL=0
export RUSTFLAGS="-Zprofile -Copt-level=0 -Clink-dead-code -Zpanic_abort_tests -Cpanic=abort"
export RUSTUP_TOOLCHAIN="nightly"
export LLVM_PROFILE_FILE="coverage-%p-%m.profraw"
cargo clean --profile dev
cargo test --all-features --tests --workspace
cargo install grcov # Keep grcov up to date
grcov . --branch --ignore-not-existing --llvm --binary-path target/debug/ --ignore "*cargo*" -t html -o target/coverage/