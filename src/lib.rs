// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Symple is a pure-Rust high-performance math library.
#![cfg_attr(docsrs, feature(doc_cfg, doc_cfg_hide, doc_auto_cfg))]
#![deny(unsafe_op_in_unsafe_fn)]

extern crate alloc;

pub mod number;

pub use number::{Natural};