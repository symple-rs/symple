// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! An arbitrary-length unsigned integer.
//!
//! Addition and multiplication by `u128`s or `Natural`s results in a `Natural`. Subtraction by the same results in an
//! `Integer`. Division results in a `Rational`. `Log` and `FirstRoot` result in `RealExpression`s.

use core::cmp::{self, Ordering};
use core::ops::{Deref};
use core::fmt;

use symple_core::{Bigit, Shift, UintSlice};
use symple_core::ops::*;
use symple_core::error::{Error, Result};

use crate::number::Natural;

impl Natural {
    /// Size of the number in bits
    #[inline]
    pub fn bits(&self) -> Shift {
        self.0.bits()
    }

    // /// Capacity of the bigit vector
    // #[inline]
    // pub fn capacity(&self) -> usize {
    //     self.0.capacity()
    // }

    // /// Creates a clone of the current number with capacity for at least `capacity` elements.
    // #[inline]
    // pub fn clone_with_capacity(&self, capacity: usize) -> Self {
    //     let trimmed = self.trimmed_slice();
    //     let mut out = Self(Vec::with_capacity(cmp::max(trimmed.len(), capacity)));
    //     out.0.extend(trimmed);
    //     out
    // }

    // /// Shrink the memory usage of the number with a lower bound
    // #[inline]
    // pub fn shrink_to(&mut self, min_capacity: usize) {
    //     self.trim();
    //     self.0.shrink_to(min_capacity)
    // }

    // /// Shrink the memory usage of the number as much as possible
    // #[inline]
    // pub fn shrink_to_fit(&mut self) {
    //     self.trim();
    //     self.0.shrink_to_fit()
    // }

    /// Remove all zeros from the end of the number
    #[inline]
    pub fn trim_zeros(&mut self) {
        self.0.trim_zeros();
    }

    /// Remove all zeros from the end of the number
    #[inline]
    pub fn trimmed_slice(&self) -> &UintSlice {
        self.0.deref().trim_zeros()
    }

    // /// Turns `self` into the absolute value of `self - rhs`. Returns `true` if overflow occurred.
    // pub fn overflowing_sub_assign(&mut self, rhs: &Self) -> bool {
    //     self.0.overflowing_sub_assign(rhs.0.0.as_slice())
    // }

    // /// Turns `self` into the absolute value of `self - rhs`. Returns `true` if overflow occurred.
    // pub fn overflowing_sub_assign_bigit(&mut self, rhs: Bigit) -> bool {
    //     self.0.overflowing_sub_assign_bigit(rhs)
    // }
}

// /* ABSOLUTE VALUE */
// impl crate::ops::Abs for Natural {
//     type Output = Natural;
//     #[inline]
//     fn abs(self) -> Self::Output {
//         self
//     }
// }

// impl crate::ops::Abs for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn abs(self) -> Self::Output {
//         self.clone()
//     }
// }

// /* ADDITION */
// impl core::ops::Add<Natural> for Bigit {
//     type Output = Natural;
//     #[inline]
//     fn add(self, rhs: Natural) -> Self::Output {
//         rhs + self
//     }
// }

// impl core::ops::Add<&Natural> for Bigit {
//     type Output = Natural;
//     #[inline]
//     fn add(self, rhs: &Natural) -> Self::Output {
//         rhs.clone_with_capacity(rhs.0.len() + 1) + self
//     }
// }

// impl core::ops::Add<Bigit> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn add(mut self, rhs: Bigit) -> Self::Output {
//         self += rhs;
//         self
//     }
// }

// impl core::ops::Add<Bigit> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn add(self, rhs: Bigit) -> Self::Output {
//         self.clone_with_capacity(self.0.len() + 1) + rhs
//     }
// }

// impl core::ops::Add<Natural> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn add(mut self, mut rhs: Natural) -> Self::Output {
//         if rhs.0.len() > self.0.len() {
//             rhs += &self;
//             rhs
//         }
//         else {
//             self += &rhs;
//             self
//         }
//     }
// }

// impl core::ops::Add<Natural> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn add(self, rhs: Natural) -> Self::Output {
//         rhs + self
//     }
// }

// impl core::ops::Add<&Natural> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn add(mut self, rhs: &Natural) -> Self::Output {
//         self += rhs;
//         self
//     }
// }

// impl core::ops::Add<&Natural> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn add(self, rhs: &Natural) -> Self::Output {
//         let mut out = self.clone_with_capacity(cmp::max(self.0.len(), rhs.0.len()) + 1);
//         out += rhs;
//         out
//     }
// }

// // impl core::ops::Add<Integer> for Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn add(self, rhs: Integer) -> Self::Output {
// //         if rhs.negative {
// //             self - rhs.value
// //         }
// //         else {
// //             (self + rhs.value).into()
// //         }
// //     }
// // }

// // impl core::ops::Add<Integer> for &Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn add(self, rhs: Integer) -> Self::Output {
// //         if rhs.negative {
// //             self - rhs.value
// //         }
// //         else {
// //             (self + rhs.value).into()
// //         }
// //     }
// // }

// // impl core::ops::Add<&Integer> for Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn add(self, rhs: &Integer) -> Self::Output {
// //         if rhs.negative {
// //             self - &rhs.value
// //         }
// //         else {
// //             (self + &rhs.value).into()
// //         }
// //     }
// // }

// // impl core::ops::Add<&Integer> for &Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn add(self, rhs: &Integer) -> Self::Output {
// //         if rhs.negative {
// //             self - &rhs.value
// //         }
// //         else {
// //             (self + &rhs.value).into()
// //         }
// //     }
// // }

// impl core::ops::AddAssign<Natural> for Bigit {
//     #[inline]
//     fn add_assign(&mut self, rhs: Natural) {
//         *self += &rhs
//     }
// }

// impl core::ops::AddAssign<&Natural> for Bigit {
//     #[inline]
//     fn add_assign(&mut self, rhs: &Natural) {
//         match rhs.trimmed_slice() {
//             &[] => return,
//             &[value] => {
//                 let overflowed;
//                 (*self, overflowed) = self.overflowing_add(value);
//                 if !overflowed {
//                     return
//                 }
//             },
//             _ => {}
//         }
//         panic!("overflow occured while attempting to add {rhs} to {self}")
//     }
// }

// impl core::ops::AddAssign<Bigit> for Natural {
//     #[inline]
//     fn add_assign(&mut self, rhs: Bigit) {
//         let overflow = BigEndianRefMut::from(&mut self.0).add_assign(rhs);
//         if overflow != 0 {
//             self.0.push(overflow);
//         }
//     }
// }

// impl core::ops::AddAssign<Natural> for Natural {
//     #[inline]
//     fn add_assign(&mut self, rhs: Natural) {
//         *self += &rhs
//     }
// }

// impl core::ops::AddAssign<&Natural> for Natural {
//     #[inline]
//     fn add_assign(&mut self, rhs: &Natural) {
//         let rhs = BigEndianRef::from(rhs).trim_zeros();
//         if self.0.len() < rhs.len() {
//             self.0.resize(rhs.len(), 0);
//         }
//         if BigEndianRefMut::from(&mut self.0).add_assign(rhs) {
//             self.0.push(1);
//         }
//     }
// }

// // impl core::ops::AddAssign<Integer> for Natural {
// //     #[inline]
// //     fn add_assign(&mut self, rhs: Integer) {
// //         *self += &rhs
// //     }
// // }

// // impl core::ops::AddAssign<&Integer> for Natural {
// //     #[inline]
// //     fn add_assign(&mut self, rhs: &Integer) {
// //         if rhs.negative {
// //             *self -= &rhs.value
// //         }
// //         else {
// //             *self += &rhs.value
// //         }
// //     }
// // }

// /* SUBTRACTION */
// // impl core::ops::Sub<Natural> for Bigit {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(self, rhs: Natural) -> Self::Output {
// //         -(rhs - self)
// //     }
// // }

// // impl core::ops::Sub<&Natural> for Bigit {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(self, rhs: &Natural) -> Self::Output {
// //         self - rhs.clone()
// //     }
// // }

// // impl core::ops::Sub<Bigit> for Natural {
// //     type Output = Integer;
// //     fn sub(mut self, rhs: Bigit) -> Self::Output {
// //         Integer {
// //             negative: self.overflowing_sub_assign_bigit(rhs),
// //             value: self,
// //         }
// //     }
// // }

// // impl core::ops::Sub<Bigit> for &Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(self, rhs: Bigit) -> Self::Output {
// //         self.clone() - rhs
// //     }
// // }

// // impl core::ops::Sub<Natural> for Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(mut self, mut rhs: Natural) -> Self::Output {
// //         if rhs.len() == 0 {
// //             return self.into()
// //         }
// //         debug_assert!(rhs.0.0[rhs.len() - 1] != 0, "{rhs:?}");
// //         if self.len() == 0 {
// //             return -rhs
// //         }
// //         debug_assert!(self.0.0[self.len() - 1] != 0, "{self:?}");
// //         if self.len() >= rhs.len() { // Minimize allocates
// //             Integer {
// //                 negative: self.0.overflowing_sub_assign(rhs.0.0.as_slice()),
// //                 value: self
// //             }
// //         }
// //         else {
// //             rhs.0.overflowing_sub_assign(self.0.0.as_slice());
// //             Integer {
// //                 negative: true,
// //                 value: rhs,
// //             }
// //         }
// //     }
// // }

// // impl core::ops::Sub<Natural> for &Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(self, rhs: Natural) -> Self::Output {
// //         -(rhs - self)
// //     }
// // }

// // impl core::ops::Sub<&Natural> for Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(mut self, rhs: &Natural) -> Self::Output {
// //         Integer {
// //             negative: self.overflowing_sub_assign(rhs),
// //             value: self,
// //         }
// //     }
// // }

// // impl core::ops::Sub<&Natural> for &Natural {
// //     type Output = Integer;
// //     fn sub(self, rhs: &Natural) -> Self::Output {
// //         if rhs.len() == 0 {
// //             return self.clone().into()
// //         }
// //         debug_assert!(rhs.0.0[rhs.len() - 1] != 0, "{rhs:?}");
// //         if self.len() == 0 {
// //             return -rhs.clone()
// //         }
// //         debug_assert!(self.0.0[self.len() - 1] != 0, "{self:?}");
// //         let (out, overflowed) = BigEndian::overflowing_sub(self.0.0.as_slice(), rhs.0.0.as_slice());
// //         Integer {
// //             negative: overflowed,
// //             value: Natural(out),
// //         }
// //     }
// // }

// // impl core::ops::Sub<Integer> for Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(self, rhs: Integer) -> Self::Output {
// //         if rhs.negative {
// //             (self + rhs.value).into()
// //         }
// //         else {
// //             self - rhs.value
// //         }
// //     }
// // }

// // impl core::ops::Sub<Integer> for &Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(self, rhs: Integer) -> Self::Output {
// //         if rhs.negative {
// //             (self + rhs.value).into()
// //         }
// //         else {
// //             self - rhs.value
// //         }
// //     }
// // }

// // impl core::ops::Sub<&Integer> for Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(self, rhs: &Integer) -> Self::Output {
// //         if rhs.negative {
// //             (self + &rhs.value).into()
// //         }
// //         else {
// //             self - &rhs.value
// //         }
// //     }
// // }

// // impl core::ops::Sub<&Integer> for &Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn sub(self, rhs: &Integer) -> Self::Output {
// //         if rhs.negative {
// //             (self + &rhs.value).into()
// //         }
// //         else {
// //             self - &rhs.value
// //         }
// //     }
// // }

// impl core::ops::SubAssign<Natural> for Bigit {
//     #[inline]
//     fn sub_assign(&mut self, rhs: Natural) {
//         *self -= &rhs
//     }
// }

// impl core::ops::SubAssign<&Natural> for Bigit {
//     #[inline]
//     fn sub_assign(&mut self, rhs: &Natural) {
//         if let Ok(bigit) = Bigit::try_from(rhs) {
//             let overflowed;
//             (*self, overflowed) = self.overflowing_sub(bigit);
//             if !overflowed {
//                 return
//             }
//         }
//         panic!("overflow occured while attempting to subtract {rhs} from {self}")
//     }
// }

// impl core::ops::SubAssign<Bigit> for Natural {
//     fn sub_assign(&mut self, rhs: Bigit) {
//         if rhs == 0 {
//             return;
//         }
//         if self.0.len() == 0 || BigEndianRefMut::from(&mut self.0).sub_assign(BigEndianRef::from(&[rhs])) {
//             panic!("overflow occured while attempting to subtract {rhs} from {self}")
//         }
//     }
// }

// impl core::ops::SubAssign<Natural> for Natural {
//     #[inline]
//     fn sub_assign(&mut self, rhs: Natural) {
//         *self -= &rhs
//     }
// }

// impl core::ops::SubAssign<&Natural> for Natural {
//     fn sub_assign(&mut self, rhs: &Natural) {
//         let rhs = BigEndianRef::from(rhs).trim_zeros();
//         if self.0.len() < rhs.len() || BigEndianRefMut::from(&mut self.0).sub_assign(rhs.into()) {
//             panic!("overflow occured while attempting to subtract {rhs} from {self}")
//         }
//     }
// }

// // impl core::ops::SubAssign<Integer> for Natural {
// //     #[inline]
// //     fn sub_assign(&mut self, rhs: Integer) {
// //         *self -= &rhs
// //     }
// // }

// // impl core::ops::SubAssign<&Integer> for Natural {
// //     #[inline]
// //     fn sub_assign(&mut self, rhs: &Integer) {
// //         if rhs.negative {
// //             *self += &rhs.value
// //         }
// //         else {
// //             *self -= &rhs.value
// //         }
// //     }
// // }

// // /* NEGATION */
// // impl core::ops::Neg for Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn neg(self) -> Self::Output {
// //         Integer {
// //             negative: self.len() > 0,
// //             value: self,
// //         }
// //     }
// // }

// /* MULTIPLICATION */
// impl core::ops::Mul<Natural> for Bigit {
//     type Output = Natural;
//     #[inline]
//     fn mul(self, mut rhs: Natural) -> Self::Output {
//         let overflow = BigEndianRefMut::from(&mut rhs.0).mul_assign(self);
//         if overflow != 0 {
//             rhs.0.push(overflow);
//         }
//         rhs
//     }
// }

// // impl core::ops::Mul<&Natural> for Bigit {
// //     type Output = Natural;
// //     fn mul(self, rhs: &Natural) -> Self::Output {
// //         if self == 0 {
// //             return Natural(Vec::new())
// //         }
// //         Natural(&rhs.0 * &[self])
// //     }
// // }

// impl core::ops::Mul<Bigit> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn mul(self, rhs: Bigit) -> Self::Output {
//         rhs * self
//     }
// }

// // impl core::ops::Mul<Bigit> for &Natural {
// //     type Output = Natural;
// //     #[inline]
// //     fn mul(self, rhs: Bigit) -> Self::Output {
// //         rhs * self
// //     }
// // }

// impl core::ops::Mul<Natural> for Natural {
//     type Output = Natural;
//     fn mul(mut self, mut rhs: Natural) -> Self::Output {
//         self.trim();
//         rhs.trim();
//         let (big, small) = if self.0.len() > rhs.0.len() { (self, rhs) } else { (rhs, self) };
//         big * &small
//     }
// }

// impl core::ops::Mul<Natural> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn mul(self, mut rhs: Natural) -> Self::Output {
//         match self.trimmed_slice() {
//             &[] => {
//                 rhs.0.clear();
//                 rhs
//             },
//             &[value] => rhs * value,
//             _ => self * &rhs
//         }
//     }
// }

// impl core::ops::Mul<&Natural> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn mul(self, rhs: &Natural) -> Self::Output {
//         rhs * self
//     }
// }

// impl core::ops::Mul<&Natural> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn mul(self, rhs: &Natural) -> Self::Output {
//         let mut out = Natural(vec![0; symple_core::number::bits_to_bigits(self.bits() + rhs.bits())]);
//         BigEndianRef::from(self).mul(rhs.into(), (&mut out).into());
//         out
//     }
// }

// // impl core::ops::Mul<Integer> for Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn mul(self, rhs: Integer) -> Self::Output {
// //         &self * &rhs
// //     }
// // }

// // impl core::ops::Mul<Integer> for &Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn mul(self, rhs: Integer) -> Self::Output {
// //         self * &rhs
// //     }
// // }

// // impl core::ops::Mul<&Integer> for Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn mul(self, rhs: &Integer) -> Self::Output {
// //         &self * rhs
// //     }
// // }

// // impl core::ops::Mul<&Integer> for &Natural {
// //     type Output = Integer;
// //     #[inline]
// //     fn mul(self, rhs: &Integer) -> Self::Output {
// //         rhs * self // Send it over to the super::Integer, where it can mess with Integer.negative
// //     }
// // }

// impl core::ops::MulAssign<Natural> for Bigit {
//     fn mul_assign(&mut self, rhs: Natural) {
//         *self *= &rhs
//     }
// }

// impl core::ops::MulAssign<&Natural> for Bigit {
//     fn mul_assign(&mut self, rhs: &Natural) {
//         if let Ok(bigit) = Bigit::try_from(rhs) {
//             let overflowed;
//             (*self, overflowed) = self.overflowing_mul(bigit);
//             if !overflowed {
//                 return
//             }
//         }
//         panic!("overflow occured while attempting to multiply {self} by {rhs}")
//     }
// }

// impl core::ops::MulAssign<Bigit> for Natural {
//     fn mul_assign(&mut self, rhs: Bigit) {
//         let overflow = BigEndianRefMut::from(&mut self.0).mul_assign(rhs);
//         if overflow != 0 {
//             self.0.push(overflow);
//         }
//     }
// }

// impl core::ops::MulAssign<Natural> for Natural {
//     #[inline]
//     fn mul_assign(&mut self, rhs: Natural) {
//         *self *= &rhs;
//     }
// }

// impl core::ops::MulAssign<&Natural> for Natural {
//     #[inline]
//     fn mul_assign(&mut self, rhs: &Natural) {
//         *self = &*self * rhs;
//     }
// }

// // impl core::ops::MulAssign<Integer> for Natural {
// //     #[inline]
// //     fn mul_assign(&mut self, rhs: Integer) {
// //         *self *= &rhs
// //     }
// // }

// // impl core::ops::MulAssign<&Integer> for Natural {
// //     #[inline]
// //     fn mul_assign(&mut self, rhs: &Integer) {
// //         if rhs.negative {
// //             super::overflow_panic("multiply")
// //         }
// //         else {
// //             *self *= &rhs.value
// //         }
// //     }
// // }

// /* SHIFT LEFT */
// impl core::ops::Shl<Natural> for Bigit {
//     type Output = Natural;
//     #[inline]
//     fn shl(self, rhs: Natural) -> Self::Output {
//         Natural::from(self) << &rhs
//     }
// }

// impl core::ops::Shl<&Natural> for Bigit {
//     type Output = Natural;
//     #[inline]
//     fn shl(self, rhs: &Natural) -> Self::Output {
//         Natural::from(self) << rhs
//     }
// }

// impl core::ops::Shl<Bigit> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn shl(mut self, rhs: Bigit) -> Self::Output {
//         self <<= rhs;
//         self
//     }
// }

// impl core::ops::Shl<Bigit> for &Natural {
//     type Output = Natural;
//     fn shl(self, rhs: Bigit) -> Self::Output {
//         let self_bits = self.bits();
//         if self_bits < rhs {
//             return Natural(Vec::new());
//         }
//         let mut out = Natural(vec![0; symple_core::number::bits_to_bigits(self_bits - rhs)]);
//         BigEndianRef::from(self).shl(rhs, (&mut out).into());
//         out
//     }
// }

// impl core::ops::Shl<Natural> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn shl(self, rhs: Natural) -> Self::Output {
//         self << &rhs
//     }
// }

// impl core::ops::Shl<Natural> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn shl(self, rhs: Natural) -> Self::Output {
//         self << &rhs
//     }
// }

// impl core::ops::Shl<&Natural> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn shl(mut self, rhs: &Natural) -> Self::Output {
//         self <<= rhs;
//         self
//     }
// }

// impl core::ops::Shl<&Natural> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn shl(self, rhs: &Natural) -> Self::Output {
//         match rhs.trimmed_slice() {
//             &[] => self.clone(),
//             &[value] => self << value,
//             _ => panic!("{rhs} exceeds the number of atoms in the universe"),
//         }
//     }
// }

// impl core::ops::ShlAssign<Natural> for Bigit {
//     #[inline]
//     fn shl_assign(&mut self, rhs: Natural) {
//         *self <<= &rhs
//     }
// }

// impl core::ops::ShlAssign<&Natural> for Bigit {
//     #[inline]
//     fn shl_assign(&mut self, rhs: &Natural) {
//         match rhs.trimmed_slice() {
//             &[] => {},
//             &[value] => *self <<= value,
//             _ => panic!("{rhs} exceeds the number of atoms in the universe"),
//         }
//     }
// }

// impl core::ops::ShlAssign<Bigit> for Natural {
//     #[inline]
//     fn shl_assign(&mut self, rhs: Bigit) {
//         BigEndianRefMut::from(self).shl_assign(rhs)
//     }
// }

// impl core::ops::ShlAssign<Natural> for Natural {
//     #[inline]
//     fn shl_assign(&mut self, rhs: Natural) {
//         *self <<= &rhs
//     }
// }

// impl core::ops::ShlAssign<&Natural> for Natural {
//     #[inline]
//     fn shl_assign(&mut self, rhs: &Natural) {
//         match rhs.trimmed_slice() {
//             &[] => {},
//             &[value] => *self <<= value,
//             _ => panic!("{rhs} exceeds the number of atoms in the universe"),
//         }
//     }
// }

// /* SHIFT RIGHT */
// impl core::ops::Shr<Natural> for Bigit {
//     type Output = Bigit;
//     #[inline]
//     fn shr(self, rhs: Natural) -> Self::Output {
//         self >> &rhs
//     }
// }

// impl core::ops::Shr<&Natural> for Bigit {
//     type Output = Bigit;
//     #[inline]
//     fn shr(self, rhs: &Natural) -> Self::Output {
//         match rhs.trimmed_slice() {
//             &[] => self,
//             &[value] => self >> value,
//             _ => 0,
//         }
//     }
// }

// impl core::ops::Shr<Bigit> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn shr(mut self, rhs: Bigit) -> Self::Output {
//         self >>= rhs;
//         self
//     }
// }

// impl core::ops::Shr<Bigit> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn shr(self, rhs: Bigit) -> Self::Output {
//         let self_bits = self.bits();
//         if rhs > self_bits {
//             return Natural(Vec::new());
//         }
//         let mut out = Natural(vec![0; symple_core::number::bits_to_bigits(self_bits - rhs)]);
//         BigEndianRef::from(self).shr(rhs, (&mut out).into());
//         out
//     }
// }

// impl core::ops::Shr<Natural> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn shr(self, rhs: Natural) -> Self::Output {
//         self >> &rhs
//     }
// }

// impl core::ops::Shr<Natural> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn shr(self, rhs: Natural) -> Self::Output {
//         self >> &rhs
//     }
// }

// impl core::ops::Shr<&Natural> for Natural {
//     type Output = Natural;
//     #[inline]
//     fn shr(mut self, rhs: &Natural) -> Self::Output {
//         self >>= rhs;
//         self
//     }
// }

// impl core::ops::Shr<&Natural> for &Natural {
//     type Output = Natural;
//     #[inline]
//     fn shr(self, rhs: &Natural) -> Self::Output {
//         match rhs.trimmed_slice() {
//             &[] => self.clone(),
//             &[value] => self >> value,
//             _ => Natural(Vec::new()),
//         }
//     }
// }

// impl core::ops::ShrAssign<Natural> for Bigit {
//     #[inline]
//     fn shr_assign(&mut self, rhs: Natural) {
//         *self = *self >> rhs;
//     }
// }

// impl core::ops::ShrAssign<&Natural> for Bigit {
//     #[inline]
//     fn shr_assign(&mut self, rhs: &Natural) {
//         *self = *self >> rhs;
//     }
// }

// impl core::ops::ShrAssign<Bigit> for Natural {
//     fn shr_assign(&mut self, rhs: Bigit) {
//         BigEndianRefMut::from(self).shr_assign(rhs)
//     }
// }

// impl core::ops::ShrAssign<Natural> for Natural {
//     #[inline]
//     fn shr_assign(&mut self, rhs: Natural) {
//         *self >>= &rhs
//     }
// }

// impl core::ops::ShrAssign<&Natural> for Natural {
//     #[inline]
//     fn shr_assign(&mut self, rhs: &Natural) {
//         match rhs.trimmed_slice() {
//             &[] => {},
//             &[value] => *self >>= value,
//             _ => self.0.clear(),
//         }
//     }
// }

// /* Division */

// /* Integer Division */

// /* COMPARISON */
// impl Eq for Natural {}

// impl PartialEq for Natural {
//     fn eq(&self, other: &Self) -> bool {
//         BigEndianRef::from(self).eq(other.into())
//     }
// }

// impl Ord for Natural {
//     fn cmp(&self, rhs: &Self) -> Ordering {
//         BigEndianRef::from(self).cmp(rhs.into())
//     }
// }

// impl PartialOrd for Natural {
//     #[inline]
//     fn partial_cmp(&self, rhs: &Self) -> Option<Ordering> {
//         Some(self.cmp(rhs))
//     }
// }

// impl PartialEq<Natural> for Bigit {
//     #[inline]
//     fn eq(&self, rhs: &Natural) -> bool {
//         self.partial_cmp(rhs).unwrap() == Ordering::Equal
//     }
// }

// impl PartialEq<Bigit> for Natural {
//     #[inline]
//     fn eq(&self, rhs: &Bigit) -> bool {
//         self.partial_cmp(rhs).unwrap() == Ordering::Equal
//     }
// }

// // impl PartialEq<Integer> for Natural {
// //     #[inline]
// //     fn eq(&self, rhs: &Integer) -> bool {
// //         self.partial_cmp(rhs).unwrap() == Ordering::Equal
// //     }
// // }

// impl PartialOrd<Natural> for Bigit {
//     #[inline]
//     fn partial_cmp(&self, rhs: &Natural) -> Option<Ordering> {
//         Some(match Bigit::try_from(rhs) {
//             Ok(value) => self.cmp(&value),
//             _ => Ordering::Less,
//         })
//     }
// }

// impl PartialOrd<Bigit> for Natural {
//     #[inline]
//     fn partial_cmp(&self, rhs: &Bigit) -> Option<Ordering> {
//         Some(match Bigit::try_from(self) {
//             Ok(value) => value.cmp(rhs),
//             _ => Ordering::Greater,
//         })
//     }
// }

// // impl PartialOrd<Integer> for Natural {
// //     #[inline]
// //     fn partial_cmp(&self, rhs: &Integer) -> Option<Ordering> {
// //         Some(if rhs.negative { Ordering::Greater } else { self.cmp(&rhs.value) })
// //     }
// // }

// /* UTILITY OPERATIONS */
// impl Clone for Natural {
//     #[inline]
//     fn clone(&self) -> Self {
//         Self::from(BigEndianRef::from(self))
//     }
// }

// impl core::hash::Hash for Natural {
//     #[inline]
//     fn hash<H: core::hash::Hasher>(&self, state: &mut H) {
//         BigEndianRef::from(self).hash(state)
//     }
// }

// impl fmt::Binary for Natural {
//     #[inline]
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         BigEndianRef::from(self).fmt(f)
//     }
// }

// impl fmt::Octal for Natural {
//     #[inline]
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         BigEndianRef::from(self).fmt(f)
//     }
// }

// impl fmt::LowerHex for Natural {
//     #[inline]
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         BigEndianRef::from(self).fmt(f)
//     }
// }

// impl fmt::UpperHex for Natural {
//     #[inline]
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         BigEndianRef::from(self).fmt(f)
//     }
// }

// impl fmt::Display for Natural {
//     #[inline]
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         BigEndianRef::from(self).fmt(f)
//     }
// }

// impl From<&BigEndian> for Natural {
//     #[inline]
//     fn from(value: &BigEndian) -> Self {
//         Self(value.trim_zeros().as_slice().into())
//     }
// }

// impl From<&mut BigEndian> for Natural {
//     #[inline]
//     fn from(value: &mut BigEndian) -> Self {
//         Self::from(&*value)
//     }
// }

// impl<'a> From<&'a Natural> for BigEndianRef<'a> {
//     #[inline]
//     fn from(number: &'a Natural) -> BigEndianRef<'a> {
//         Self::from(&number.0)
//     }
// }

// impl<'a> From<&'a mut Natural> for BigEndianRef<'a> {
//     #[inline]
//     fn from(number: &'a mut Natural) -> BigEndianRef<'a> {
//         Self::from(&number.0)
//     }
// }

// impl<'a> From<&'a mut Natural> for BigEndianRefMut<'a> {
//     #[inline]
//     fn from(number: &'a mut Natural) -> BigEndianRefMut<'a> {
//         Self::from(&mut number.0)
//     }
// }

// impl TryFrom<Natural> for Bigit {
//     type Error = Error;
//     #[inline]
//     fn try_from(natural: Natural) -> Result<Bigit> {
//         (&natural).try_into()
//     }
// }

// impl TryFrom<&Natural> for Bigit {
//     type Error = Error;
//     #[inline]
//     fn try_from(natural: &Natural) -> Result<Bigit> {
//         match natural.trimmed_slice() {
//             &[] => Ok(0),
//             &[value] => Ok(value),
//             _ => Err(Error::TryFromError)
//         }
//     }
// }

// impl TryFrom<Natural> for SignedBigit {
//     type Error = Error;
//     #[inline]
//     fn try_from(natural: Natural) -> Result<Self> {
//         (&natural).try_into()
//     }
// }

// impl TryFrom<&Natural> for SignedBigit {
//     type Error = Error;
//     #[inline]
//     fn try_from(natural: &Natural) -> Result<Self> {
//         match symple_core::number::to_signed(false, Bigit::try_from(natural)?) {
//             Ok(num) => Ok(num),
//             Err(err) => Err(err.into()),
//         }
//     }
// }

// impl From<Bigit> for Natural {
//     #[inline]
//     fn from(int: Bigit) -> Self {
//         Self(if int == 0 { Vec::new() } else { vec![int] })
//     }
// }

// impl TryFrom<SignedBigit> for Natural {
//     type Error = Error;
//     #[inline]
//     fn try_from(int: SignedBigit) -> Result<Self> {
//         if int < 0 {
//             Err(Error::TryFromError)
//         }
//         else {
//             Ok(Natural::from(int as Bigit))
//         }
//     }
// }

// impl TryFrom<Integer> for Natural {
//     type Error = Error;
//     #[inline]
//     fn try_from(integer: Integer) -> Result<Self> {
//         if integer.negative {
//             Err(Error::TryFromError)
//         }
//         else {
//             Ok(integer.value)
//         }
//     }
// }
