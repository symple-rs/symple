// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Error types

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
#[cfg_attr(any(test, feature = "fmt"), derive(Debug))]
pub enum Error {
    TryFromError,
}

#[cfg(feature = "fmt")]
impl core::fmt::Display for Error {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::TryFromError => write!(f, "TryFromError")
        }
    }
}

// Ergonomics for std users
#[cfg(feature = "std")]
impl std::error::Error for super::Error {}
