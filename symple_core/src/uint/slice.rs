// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Core algorithms for operating on unsigned integer slices.

use core::cmp::Ordering;
use core::mem::{self, MaybeUninit};
#[cfg(all(feature = "alloc", feature = "fmt"))]
use core::fmt::{self, Write};
use core::hash::Hash;
use core::hint::unreachable_unchecked;
use core::ops::*;

#[cfg(all(feature = "alloc", feature = "fmt"))]
use alloc::vec;
#[cfg(all(feature = "alloc", feature = "fmt"))]
use alloc::vec::Vec;

use crate::{Bigit, HalfBigit, Shift, MaybeUninitMut, MaybeUninitTrait};
#[cfg(all(feature = "alloc", feature = "fmt"))]
use crate::{HALF_BIGIT_TEN, HALF_BIGIT_DIGITS};
use crate::ops::*;
use crate::uint::*;

impl MaybeUninitUintSlice {
    /// Const version of the `From<&[Bigit]>` impl
    #[inline(always)]
    #[must_use]
    pub const fn from_slice<'a>(slice: &'a [MaybeUninit<Bigit>]) -> &'a Self {
        // SAFETY:
        // - the input and output types are equivalent, [Bigit]
        // - type safety is ensured by the return value of the function
        // - the input and output pointers are both immutable, and have the same
        //   lifetime
        // - repr(transparent) ensures the types have the same layout
        unsafe { mem::transmute(slice) }
    }

    /// Length of the inner slice
    #[inline(always)]
    #[must_use]
    pub const fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns an iterator over the slice
    #[inline(always)]
    #[must_use]
    pub fn iter<'a>(&'a self) -> core::slice::Iter<'a, MaybeUninit<Bigit>> {
        self.into_iter()
    }

    /// Returns a mutable iterator over the slice
    #[inline(always)]
    #[must_use]
    pub fn iter_mut<'a>(&'a mut self) -> core::slice::IterMut<'a, MaybeUninit<Bigit>> {
        self.into_iter()
    }
}

impl<'a> From<&'a [MaybeUninit<Bigit>]> for &'a MaybeUninitUintSlice {
    #[inline(always)]
    #[must_use]
    fn from(value: &'a [MaybeUninit<Bigit>]) -> Self {
        MaybeUninitUintSlice::from_slice(value)
    }
}

impl<'a> From<&'a mut [MaybeUninit<Bigit>]> for &'a mut MaybeUninitUintSlice {
    #[inline(always)]
    #[must_use]
    fn from(slice: &'a mut [MaybeUninit<Bigit>]) -> Self {
        // SAFETY:
        // - the input and output types are equivalent, [Bigit]
        // - type safety is ensured by the return value of the function
        // - the input and output pointers are both mutable, and have the same
        //   lifetime
        // - repr(transparent) ensures the types have the same layout
        unsafe { mem::transmute(slice) }
    }
}

impl MaybeUninitMut for MaybeUninitUintSlice {
    type Initialized = UintSlice;
    /// # Safety
    /// It is up to the caller to guarantee that the `MaybeUninit<T>` elements
    /// really are in an initialized state.
    /// Calling this when the content is not yet fully initialized causes
    /// undefined behavior.
    #[inline(always)]
    #[must_use]
    unsafe fn assume_init<'a>(&'a mut self) -> &'a mut Self::Initialized {
        // Safety: Guaranteed by caller
        <&mut UintSlice>::from(unsafe { &mut *((&mut self.0) as *mut [MaybeUninit<Bigit>] as *mut [Bigit]) })
    }
}


impl UintSlice {
    // /// Returns a reference to the inner `&[Bigit]`
    // #[inline(always)]
    // #[must_use]
    // pub const fn as_slice(&self) -> &[Bigit] {
    //     &self.0
    // }

    // /// Returns a mutable reference to the inner `&[Bigit]`
    // #[inline(always)]
    // #[must_use]
    // pub fn as_mut_slice(&mut self) -> &mut [Bigit] {
    //     &mut self.0
    // }

    /// The number of bits required to represent the number.
    ///
    /// For any [`UintSlice`] called `slice`,
    /// `bits_to_len(slice.bits()) == slice.trim_zeros().len()`.
    #[inline]
    #[must_use]
    pub const fn bits(&self) -> Shift {
        let mut trimmed = &self.0;
        while let [left @ .., elem] = trimmed {
            if *elem != 0 {
                return ((trimmed.len() as Shift) << Bigit::BITS.trailing_zeros()) - (elem.leading_zeros() as Shift)
            }
            trimmed = left;
        }
        0
    }

    /// Const version of the `From<&[Bigit]>` impl
    #[inline(always)]
    #[must_use]
    pub const fn from_slice<'a>(slice: &'a [Bigit]) -> &'a Self {
        // SAFETY:
        // - the input and output types are equivalent, [Bigit]
        // - type safety is ensured by the return value of the function
        // - the input and output pointers are both immutable, and have the same
        //   lifetime
        // - repr(transparent) ensures the types have the same layout
        unsafe { mem::transmute(slice) }
    }

    /// Returns an iterator over the slice
    #[inline(always)]
    #[must_use]
    pub fn iter<'a>(&'a self) -> core::slice::Iter<'a, Bigit> {
        self.into_iter()
    }

    /// Returns a mutable iterator over the slice
    #[inline(always)]
    #[must_use]
    pub fn iter_mut<'a>(&'a mut self) -> core::slice::IterMut<'a, Bigit> {
        self.into_iter()
    }

    /// Returns a reference to the last element of the slice, if it exists
    #[inline(always)]
    #[must_use]
    pub const fn last(&self) -> Option<&Bigit> {
        self.0.last()
    }

    /// Returns a mutable reference to the last element of the slice, if it
    /// exists
    #[inline(always)]
    #[must_use]
    pub fn last_mut(&mut self) -> Option<&mut Bigit> {
        self.0.last_mut()
    }

    /// Length of the inner slice
    #[inline(always)]
    #[must_use]
    pub const fn len(&self) -> usize {
        self.0.len()
    }

    /// Returns a reference to the current slice, minus any trailing zeros
    #[inline]
    #[must_use]
    pub const fn trim_zeros<'a>(&'a self) -> &'a Self {
        let mut trimmed = &self.0;
        while let [left @ .., 0] = trimmed {
            trimmed = left;
        }
        Self::from_slice(trimmed)
    }

    /// Returns a mutable reference to the current slice, minus any trailing
    /// zeros
    #[inline]
    #[must_use]
    pub fn trim_zeros_mut<'a>(&'a mut self) -> &'a mut Self {
        let mut trimmed = &mut self.0;
        while let [left @ .., 0] = trimmed {
            trimmed = left;
        }
        trimmed.into()
    }

    /// Zeroes out current slice.
    #[inline(always)]
    pub fn zero(&mut self) {
        self.iter_mut().for_each(|x| *x = 0);
    }

    /// The minimum output slice length required to do subtraction.
    ///
    /// Suppose one is subtracting 12000 from 12345. The first two digits are
    /// identical, therefore the output number is guaranteed to be no more than
    /// three digits long. This function does that calculation for
    /// [`UintSlice`]s--given a pair of slices `[5, 4, 3, 2, 1]` and
    /// `[0, 0, 0, 2, 1]`, it returns `3`.
    ///
    /// ```
    /// use symple_core::{Bigit, UintSlice, UintArray};
    /// use symple_core::ops::SubInto;
    /// use symple_core::uint::MaybeUninitUintArray;
    ///
    /// const LHS: &UintSlice = UintArray::from_array([4, 3, 3]).as_slice();
    /// const RHS: &UintSlice = UintArray::from_array([1, 2, 3]).as_slice();
    ///
    /// const OUT_LEN: usize = LHS.sub_len(RHS);
    /// assert_eq!(OUT_LEN, 2); // Output length is shorter than the inputs
    ///
    /// // Despite the output slice being shorter than either input, subtraction still works
    /// let out = MaybeUninitUintArray::<OUT_LEN>::uninit();
    /// assert_eq!(unsafe { LHS.sub_into(RHS, out) }, (UintArray::from([3, 1]), false));
    ///
    /// // To optimize further, remove excess length from the inputs before subtraction
    /// let out = MaybeUninitUintArray::<OUT_LEN>::uninit();
    /// assert_eq!(unsafe { RHS[..OUT_LEN].sub_into(&LHS[..OUT_LEN], out) }, (UintArray::from([3, 1]), true));
    /// ```
    ///
    /// Note that subtraction may still yield trailing zeros. 100 - 99 == 001
    ///
    /// ```
    /// use symple_core::{Bigit, UintSlice, UintArray};
    /// use symple_core::ops::SubInto;
    /// use symple_core::uint::MaybeUninitUintArray;
    ///
    /// const LHS: &UintSlice = UintArray::from_array([0, 0, 1]).as_slice();
    /// const RHS: &UintSlice = UintArray::from_array([Bigit::MAX, Bigit::MAX]).as_slice();
    ///
    /// const OUT_LEN: usize = LHS.sub_len(RHS);
    /// assert_eq!(OUT_LEN, 3);
    ///
    /// let out = MaybeUninitUintArray::<OUT_LEN>::uninit();
    /// assert_eq!(unsafe { LHS.sub_into(RHS, out) }, (UintArray::from([1, 0, 0]), false));
    /// ```
    #[inline]
    #[must_use]
    pub const fn sub_len(&self, other: &UintSlice) -> usize {
        let (mut big, mut small) = if self.len() > other.len() { (&self.0, &other.0) } else { (&other.0, &self.0) };
        while big.len() > small.len() {
            if let [inner_big @ .., 0] = big {
                big = inner_big
            }
            else {
                return big.len()
            }
        }
        while let [big_start @ .., big_end] = big {
            match small {
                [small_start @ .., small_end] if *small_end == *big_end => {
                    big = big_start;
                    small = small_start;
                }
                _ => break
            }
        }
        big.len()
    }

    /// Adds `other` to `self` in place and returns any overflow.
    #[inline]
    pub fn add_assign(&mut self, mut other: Bigit) -> Bigit {
        for elem in self.iter_mut() {
            let carry;
            (*elem, carry) = elem.overflowing_add(other);
            if !carry {
                return 0
            }
            other = 1;
        }
        other
    }

    /// Adds the `other` to `self` in place and returns `true` if overflow
    /// occurred.
    ///
    /// # Safety
    /// `self.len()` must be greater than or equal to `other.len()`
    pub unsafe fn add_assign_slice(&mut self, other: &UintSlice) -> bool {
        debug_assert!(self.len() >= other.len());
        let mut carry = false;
        let mut i = 0;
        while i < other.len() {
            // SAFETY: guaranteed by caller
            let self_elem = unsafe { self.get_unchecked_mut(i) };
            let new_carry;
            // SAFETY: `i < other.len()` by inspection
            (*self_elem, new_carry) = self_elem.overflowing_add( unsafe { *other.get_unchecked(i) });
            if carry {
                (*self_elem, carry) = self_elem.overflowing_add(1);
            }
            carry |= new_carry;
            i += 1;
        }
        while carry && i < self.len() {
            // SAFETY: `i < self.len()` by inspection
            let elem = unsafe { self.get_unchecked_mut(i) };
            (*elem, carry) = elem.overflowing_add(1);
            i += 1;
        }
        carry
    }

    /// Calculates the absolute value of `self - other` in place. Returns `true`
    /// if `other > self`.
    ///
    /// # Safety
    /// `self.len()` must be greater than or equal to `other.len()`
    pub unsafe fn sub_assign_slice(&mut self, mut other: &UintSlice) -> bool {
        debug_assert!(self.len() >= other.len());
        if other.len() == 0 {
            return false
        }
        let mut overflow = true;
        if self.len() > other.len() {
            for elem in unsafe { self.get_unchecked(other.len()..) }.iter().rev() {
                if elem != &0 {
                    overflow = false;
                    break
                }
            }
        }
        if overflow {
            overflow = false; // If self == other, overflow must be false
            for (mut i, (self_elem, other_elem)) in self.iter().zip(&other.0).enumerate().rev() {
                if self_elem != other_elem {
                    overflow = self_elem < other_elem;
                    i += 1;
                    // Everything above other.len() is known to be zero already
                    unsafe { self.get_unchecked_mut(i..other.len()) }.iter_mut().for_each(|x| *x = 0);
                    other = unsafe { other.get_unchecked(..i) }.into();
                    break
                }
            }
        }
        let mut carry = false;
        // Could place this case structure inside the for loop, reducing code
        // size, but it would incur a small cost to performance
        if overflow {
            for (self_elem, other_elem) in self.iter_mut().zip(&other.0) {
                let new_carry;
                (*self_elem, new_carry) = other_elem.overflowing_sub(*self_elem);
                if carry {
                    (*self_elem, carry) = self_elem.overflowing_sub(1);
                }
                carry |= new_carry;
            }
        }
        else {
            for (self_elem, other_elem) in self.iter_mut().zip(&other.0) {
                let new_carry;
                (*self_elem, new_carry) = self_elem.overflowing_sub(*other_elem);
                if carry {
                    (*self_elem, carry) = self_elem.overflowing_sub(1);
                }
                carry |= new_carry;
            }
        }
        if carry {
            for self_elem in unsafe { self.get_unchecked_mut(other.len()..) }.iter_mut() {
                (*self_elem, carry) = self_elem.overflowing_sub(1);
                if !carry {
                    break
                }
            }
        }
        overflow
    }

    #[inline(always)]
    fn mul_assign_halfbigit(&mut self, other: HalfBigit) -> HalfBigit {
        let mut carry = 0;
        for elem in self.iter_mut() {
            let (lower, upper) = crate::widening_mul_half(other, *elem);
            let add_to_carry;
            (*elem, add_to_carry) = lower.overflowing_add(carry);
            carry = upper;
            if add_to_carry {
                carry += 1;
            }
        }
        if carry > HalfBigit::MAX as Bigit { unsafe { unreachable_unchecked() } }
        carry as HalfBigit
    }

    /// Multiplies `self` by `other` in place and returns any overflow.
    pub fn mul_assign(&mut self, other: HalfBigit) -> HalfBigit {
        self.mul_assign_halfbigit(other)
    }

    /// Multiplies `self` by `other` in place and returns any overflow.
    ///
    /// Slower than plain `mul_assign`, but accepts a standard `Bigit`.
    pub fn mul_assign_bigit(&mut self, other: Bigit) -> Bigit {
        let other_lower = other as HalfBigit;
        if other <= HalfBigit::MAX as Bigit {
            self.mul_assign_halfbigit(other_lower) as Bigit
        }
        else {
            let other_upper = (other >> HalfBigit::BITS) as HalfBigit;
            let mut carry = 0;
            for elem in self.iter_mut() {
                let (lower, upper) = crate::widening_mul_split(other_lower, other_upper, *elem);
                let add_to_carry;
                (*elem, add_to_carry) = lower.overflowing_add(carry);
                carry = upper;
                if add_to_carry {
                    carry += 1;
                }
            }
            carry
        }
    }

    /// Schoolbook multiplication algorithm
    ///
    /// Adds `self * other` to any existing value in `output`
    ///
    /// # Safety
    /// `self * other + output` must not overflow the length of `output`
    unsafe fn mul_base(&self, other: &UintSlice, output: &mut UintSlice) {
        let other = other.trim_zeros();
        for (self_index, self_bigit) in self.iter().enumerate() {
            let self_bigit = *self_bigit;
            if self_bigit == 0 {
                continue
            }
            let self_lower = self_bigit as HalfBigit;
            if self_bigit > HalfBigit::MAX as Bigit {
                let self_upper = (self_bigit >> HalfBigit::BITS) as HalfBigit;
                for (rhs_index, rhs_bigit) in other.iter().enumerate() {
                    let (mut lower, mut upper) = crate::widening_mul_split(self_lower, self_upper, *rhs_bigit);
                    let out_index = self_index + rhs_index;
                    let mut i = out_index;
                    let mut carry;
                    loop {
                        // SAFETY: guaranteed by caller
                        debug_assert!(i < output.len());
                        let out_elem = unsafe { output.get_unchecked_mut(i) };
                        (*out_elem, carry) = out_elem.overflowing_add(lower);
                        if !carry {
                            break
                        }
                        lower = 1;
                        i += 1;
                    }
                    i = out_index + 1;
                    loop {
                        // SAFETY: guaranteed by caller
                        debug_assert!(i < output.len());
                        let out_elem = unsafe { output.get_unchecked_mut(i) };
                        (*out_elem, carry) = out_elem.overflowing_add(upper);
                        if !carry {
                            break
                        }
                        upper = 1;
                        i += 1;
                    }
                }
            }
            else {
                for (rhs_index, rhs_bigit) in other.iter().enumerate() {
                    let (mut lower, mut upper) = crate::widening_mul_half(self_lower, *rhs_bigit);
                    let out_index = self_index + rhs_index;
                    let mut i = out_index;
                    let mut carry;
                    loop {
                        // SAFETY: guaranteed by caller
                        debug_assert!(i < output.len());
                        let out_elem = unsafe { output.get_unchecked_mut(i) };
                        (*out_elem, carry) = out_elem.overflowing_add(lower);
                        if !carry {
                            break
                        }
                        lower = 1;
                        i += 1;
                    }
                    i = out_index + 1;
                    loop {
                        // SAFETY: guaranteed by caller
                        debug_assert!(i < output.len());
                        let out_elem = unsafe { output.get_unchecked_mut(i) };
                        (*out_elem, carry) = out_elem.overflowing_add(upper);
                        if !carry {
                            break
                        }
                        upper = 1;
                        i += 1;
                    }
                }
            }
        }
    }

    /// Divides `self` by `other` in place and returns the remainder
    ///
    /// # Panics
    /// Panics if `other == 0`.
    #[inline]
    pub fn div_rem_assign(&mut self, other: HalfBigit) -> HalfBigit {
        let other = other as Bigit;
        let mut remainder = 0;
        for elem in self.iter_mut().rev() {
            let original_elem = *elem;
            // Divide top 64 bits of self_elem
            remainder <<= HalfBigit::BITS;
            remainder |= original_elem >> HalfBigit::BITS;
            // TODO: revisit this if Rust adds div_rem intrinsics
            *elem = (remainder/other) << HalfBigit::BITS;
            remainder %= other;
            // Divide bottom 64 bits of self_elem
            remainder <<= HalfBigit::BITS;
            remainder |= original_elem & HalfBigit::MAX as Bigit;
            // TODO: Also revisit this
            *elem |= remainder/other;
            remainder %= other;
        }
        if remainder > HalfBigit::MAX as Bigit { unsafe { unreachable_unchecked() } }
        remainder as HalfBigit
    }
}

impl Ord for UintSlice {
    #[must_use]
    fn cmp(&self, other: &UintSlice) -> Ordering {
        let mut lhs = &self.0;
        let mut rhs = &other.0;
        match lhs.len().cmp(&rhs.len()) {
            Ordering::Less => loop {
                // SAFETY:
                // At start of loop `rhs.len() > lhs.len()`, therefore
                // `rhs.len() > 0`. That condition is re-asserted at the end
                // of every iteration of the loop.
                if rhs.len() == 0 { unsafe { unreachable_unchecked() } }
                if let [rhs_inner @ .., 0] = rhs {
                    rhs = rhs_inner;
                }
                else {
                    return Ordering::Less
                }
                if lhs.len() == rhs.len() {
                    break
                }
            },
            Ordering::Equal => {},
            Ordering::Greater => loop {
                // SAFETY:
                // At start of loop `lhs.len() > rhs.len()`, therefore
                // `lhs.len() > 0`. That condition is re-asserted at the end
                // of every iteration of the loop.
                if lhs.len() == 0 { unsafe { unreachable_unchecked() } }
                if let [lhs_inner @ .., 0] = lhs {
                    lhs = lhs_inner;
                }
                else {
                    return Ordering::Greater
                }
                if lhs.len() == rhs.len() {
                    break
                }
            },
        }
        // SAFETY:
        // Every path leading to this code requires lhs.len() == rhs.len()
        if lhs.len() != rhs.len() { unsafe { unreachable_unchecked() } }
        for (left, right) in lhs.iter().zip(rhs).rev() {
            match left.cmp(right) {
                Ordering::Equal => {},
                ord => return ord,
            }
        }
        Ordering::Equal
    }
}

impl ShlAssign<Shift> for UintSlice {
    /// Shifts `self` left by `rhs` bits in place.
    ///
    /// Any bits that do not fit are truncated.
    #[must_use]
    fn shl_assign(&mut self, rhs: Shift) {
        if rhs == 0 {
            return
        }
        let (major, minor) = crate::shift_to_parts(rhs);
        if major >= self.len() {
            self.iter_mut().for_each(|i| *i = 0);
            return
        }
        let mut upper_index = self.len();
        let mut lower_index = upper_index - major;
        if minor == 0 {
            while lower_index > 0 {
                upper_index -= 1;
                lower_index -= 1;
                let lower = unsafe { *self.get_unchecked(lower_index) };
                let upper_ref = unsafe { self.get_unchecked_mut(upper_index) };
                *upper_ref = lower;
            }
        }
        else {
            let right_shift = Bigit::BITS - minor;
            lower_index -= 1;
            loop {
                upper_index -= 1;
                let lower = unsafe { *self.get_unchecked(lower_index) };
                let upper_ref = unsafe { self.get_unchecked_mut(upper_index) };
                *upper_ref = lower << minor;
                if lower_index == 0 {
                    break
                }
                lower_index -= 1;
                let lower = unsafe { *self.get_unchecked(lower_index) };
                let upper_ref = unsafe { self.get_unchecked_mut(upper_index) };
                *upper_ref |= lower >> right_shift;
            }
        }
        // SAFETY:
        // `major < self.len()` by the conditional after `major` is declared
        unsafe { self.get_unchecked_mut(..major) }.iter_mut().for_each(|i| *i = 0);
    }
}

impl ShrAssign<Shift> for UintSlice {
    /// Shifts `self` right by `rhs` bits in place.
    #[must_use]
    fn shr_assign(&mut self, rhs: Shift) {
        if rhs == 0 {
            return
        }
        let (mut major, minor) = crate::shift_to_parts(rhs);
        if major >= self.len() {
            self.iter_mut().for_each(|i| *i = 0);
            return
        }
        let end_len = self.len() - major;
        if minor == 0 {
            for lower_index in 0..end_len {
                let upper = unsafe { *self.get_unchecked(major) };
                let lower_ref = unsafe { self.get_unchecked_mut(lower_index) };
                *lower_ref = upper;
                major += 1;
            }
        }
        else {
            let left_shift = Bigit::BITS - minor;
            for lower_index in 0.. {
                let upper = unsafe { *self.get_unchecked(major) };
                let lower_ref = unsafe { self.get_unchecked_mut(lower_index) };
                *lower_ref = upper >> minor;
                major += 1;
                if major == self.len() {
                    break
                }
                let upper = unsafe { *self.get_unchecked(major) };
                let lower_ref = unsafe { self.get_unchecked_mut(lower_index) };
                *lower_ref |= upper << left_shift;
            }
        }
        unsafe { self.get_unchecked_mut(end_len..) }.iter_mut().for_each(|i| *i = 0);
    }
}

impl ShlInto<Shift> for UintSlice {
    type MaybeUninitSlice = MaybeUninitUintSlice;
    /// Left shift operation
    ///
    /// Any bits that do not fit into `output` are truncated.
    #[must_use]
    fn shl_into<M>(&self, other: Shift, mut output: M) -> M::Initialized
    where
        M: MaybeUninitTrait + Deref<Target = Self::MaybeUninitSlice> + DerefMut
    {
        let out_slice = &mut *output;
        let (mut major, minor) = crate::shift_to_parts(other);
        if major >= out_slice.len() {
            out_slice.iter_mut().for_each(|x| { x.write(0); });
            // SAFETY: All elements of out_slice have been initialized to 0
            return unsafe { output.assume_init() }
        }
        // SAFETY:
        // `major < out.len()` by the conditional above
        unsafe { out_slice.get_unchecked_mut(..major) }.iter_mut().for_each(|x| { x.write(0); });
        if minor == 0 {
            // SAFETY:
            // `major < out.len()` by the conditional above
            for (out_elem, self_elem) in unsafe { out_slice.get_unchecked_mut(major..) }.iter_mut().zip(&self.0) {
                out_elem.write(*self_elem);
            }
            if let Some(subslice) = out_slice.get_mut(major + self.len()..) {
                subslice.iter_mut().for_each(|x| { x.write(0); });
            }
            // SAFETY:
            // All elements have been initialized. The `..major` values were
            // set before the first loop, the `major..major + self.len()`
            // values were set in the first loop, and all remaining elements
            // were set to zero in the second loop.
        }
        else {
            let right_shift = Bigit::BITS - minor;
            // SAFETY:
            // `major < out.len()` by the conditional above
            let mut out_ref = unsafe { out_slice.get_unchecked_mut(major) };
            let mut out_value = 0;
            for elem in &self.0 {
                out_ref.write(out_value | (elem << minor));
                major += 1;
                if major >= out_slice.len() {
                    // SAFETY:
                    // All elements of out_slice have been initialized. The
                    // values below `major` are set to zero, all others have
                    // been set in this loop
                    return unsafe { output.assume_init() }
                }
                // SAFETY:
                // `major < out.len()` by the condition above
                out_ref = unsafe { out_slice.get_unchecked_mut(major) };
                out_value = elem >> right_shift;
            }
            out_ref.write(out_value);
            // SAFETY:
            // `major < out.len()` because it was so before the loop started,
            // and if it became otherwise during the loop the function returned.
            unsafe { out_slice.get_unchecked_mut(major + 1..) }.iter_mut().for_each(|x| { x.write(0); });
            // SAFETY:
            // All elements have been initialized. The `..major` values were
            // set before the loop, the `major..major + self.len()` values
            // were set in the loop, the `major + self.len()` element was
            // set just after the loop, and all elements above that were set
            // to zero.
        }
        // SAFETY: specified at the end of each possible path to this function
        unsafe { output.assume_init() }
    }
}

impl ShrInto<Shift> for UintSlice {
    type MaybeUninitSlice = MaybeUninitUintSlice;
    /// Right shift operation
    ///
    /// Any bits that do not fit into `output` are truncated.
    #[must_use]
    fn shr_into<M>(&self, other: Shift, mut output: M) -> M::Initialized
        where
            M: MaybeUninitTrait + Deref<Target = MaybeUninitUintSlice> + DerefMut {
        let out_slice = &mut *output;
        let (mut major, minor) = crate::shift_to_parts(other);
        if major >= self.len() {
            out_slice.iter_mut().for_each(|x| { x.write(0); });
            // SAFETY: All elements of out_slice have been initialized to 0
        }
        else if minor == 0 {
            // SAFETY:
            // major < self.len() by the conditional above
            let input = unsafe { self.get_unchecked(major..) };
            for (out_elem, self_elem) in out_slice.iter_mut().zip(input) {
                out_elem.write(*self_elem);
            }
            if let Some(subslice) = out_slice.get_mut(input.len()..) {
                subslice.iter_mut().for_each(|x| { x.write(0); });
            }
            // SAFETY:
            // All elements have been initialized. The `..input.len()` values
            // were set in the first loop, and all remaining elements were set
            // to zero in the second loop.
        }
        else {
            let left_shift = Bigit::BITS - minor;
            // SAFETY: `major < self.len()` by the condition above.
            let mut self_elem = unsafe { self.get_unchecked(major) };
            for (i, out_ref) in out_slice.iter_mut().enumerate() {
                let elem = self_elem >> minor;
                major += 1;
                if major == self.len() {
                    out_ref.write(elem);
                    // SAFETY: by inspection
                    unsafe { out_slice.get_unchecked_mut(i + 1..) }.iter_mut().for_each(|x| { x.write(0); });
                    break
                }
                // SAFETY:  `major < self.len()` by the condition above.
                self_elem = unsafe { self.get_unchecked(major) };
                out_ref.write(elem | (self_elem << left_shift));
            }
            // SAFETY:
            // If the for loop ended normally, all elements were written. If the
            // break occurred, all elements up to i were written normally and
            // all elements i + 1 and up were made zeros, therefore all elements
            // were written.
        }
        // SAFETY: specified at the end of each possible path to this function
        unsafe { output.assume_init() }
    }
}

impl AddInto for UintSlice {
    type MaybeUninitSlice = MaybeUninitUintSlice;
    /// Adds the `other` to `self` and places the result in `output`.
    /// Returns a tuple of the initialized `output` and the overflow state.
    ///
    /// # Safety
    /// `self.len()` and `other.len()` must be less than or equal to `out.len()`
    #[must_use]
    unsafe fn add_into<M>(&self, other: &Self, mut output: M) -> (M::Initialized, bool)
    where
        M: MaybeUninitTrait + Deref<Target = Self::MaybeUninitSlice> + DerefMut
    {
        let output_slice = &mut *output;
        debug_assert!(self.len() <= output_slice.len());
        debug_assert!(other.len() <= output_slice.len());
        let (big, small) = if self.len() > other.len() { (&self.0, &other.0) } else { (&other.0, &self.0) };
        let mut carry = false;
        let mut i = 0;
        while i < small.len() {
            // SAFETY: `i < small.len()` by inspection, `i < big.len()` by initial if statement
            let (mut out_elem, new_carry) = unsafe { small.get_unchecked(i).overflowing_add(*big.get_unchecked(i)) };
            if carry {
                (out_elem, carry) = out_elem.overflowing_add(1);
            }
            // SAFETY: guaranteed by caller
            unsafe { output_slice.get_unchecked_mut(i) }.write(out_elem);
            carry |= new_carry;
            i += 1;
        }
        while carry && i < big.len() {
            let out_elem;
            // SAFETY: `i < big.len()` by inspection
            (out_elem, carry) = unsafe { big.get_unchecked(i) }.overflowing_add(1);
            unsafe { output_slice.get_unchecked_mut(i) }.write(out_elem);
            i += 1;
        }
        if carry && i < output_slice.len() {
            unsafe { output_slice.get_unchecked_mut(i) }.write(1);
            carry = false;
            i += 1;
        }
        else {
            while i < big.len() {
                // SAFETY: guaranteed by caller, and by inspection
                unsafe { output_slice.get_unchecked_mut(i).write(*big.get_unchecked(i)) };
                i += 1;
            }
        }
        while i < output_slice.len() {
            // SAFETY: `i < out.len()` by inspection
            unsafe { output_slice.get_unchecked_mut(i) }.write(0);
            i += 1;
        }
        // SAFETY: Every element has been initialized, by inspection
        (unsafe { output.assume_init() }, carry)
    }
}

impl SubInto for UintSlice {
    type MaybeUninitSlice = MaybeUninitUintSlice;
    /// Places the absolute value of `self - rhs` in `output`. Returns `true` if
    /// subtraction overflowed.
    ///
    /// # Safety
    /// `out.len()` must be greater than or equal to `self.sub_len(rhs)`
    #[must_use]
    unsafe fn sub_into<M>(&self, other: &Self, mut output: M) -> (M::Initialized, bool)
    where
        M: MaybeUninitTrait + Deref<Target = Self::MaybeUninitSlice> + DerefMut,
        M::Initialized: Deref<Target = <Self::MaybeUninitSlice as MaybeUninitMut>::Initialized> + DerefMut
    {
        let output_slice = output.deref_mut();
        debug_assert!(output_slice.len() >= self.sub_len(other));
        let lhs = &self.trim_zeros().0;
        let rhs = &other.trim_zeros().0;
        let (overflow, big, small) = match lhs.len().cmp(&rhs.len()) {
            Ordering::Less => {
                for i in lhs.len()..rhs.len() {
                    // SAFETY: guaranteed by caller
                    unsafe { output_slice.get_unchecked_mut(i).write(*rhs.get_unchecked(i)) };
                }
                // SAFETY: guaranteed by caller
                unsafe { output_slice.get_unchecked_mut(rhs.len()..) }.iter_mut().for_each(|x| { x.write(0); });
                (true, rhs, lhs)
            },
            Ordering::Equal => 'outer: {
                for (i, (lhs_elem, rhs_elem)) in lhs.iter().zip(rhs).enumerate().rev() {
                    match lhs_elem.cmp(&rhs_elem) {
                        Ordering::Greater => {
                            // SAFETY: by inspection
                            break 'outer (false, unsafe { lhs.get_unchecked(..=i) }, unsafe { rhs.get_unchecked(..=i) })
                        },
                        Ordering::Less => {
                            // SAFETY: by inspection
                            break 'outer (true, unsafe { rhs.get_unchecked(..=i) }, unsafe { lhs.get_unchecked(..=i) })
                        },
                        Ordering::Equal => {},
                    }
                }
                output_slice.iter_mut().for_each(|x| { x.write(0); });
                // SAFETY: just set all values to zero
                return (unsafe { output.assume_init() }, false)
            },
            Ordering::Greater => {
                for i in rhs.len()..lhs.len() {
                    // SAFETY: guaranteed by caller
                    unsafe { output_slice.get_unchecked_mut(i).write(*lhs.get_unchecked(i)) };
                }
                // SAFETY: guaranteed by caller
                unsafe { output_slice.get_unchecked_mut(lhs.len()..) }.iter_mut().for_each(|x| { x.write(0); });
                (false, lhs, rhs)
            },
        };
        debug_assert!(small.len() <= big.len());
        debug_assert!(small.len() <= output_slice.len());
        let mut carry = false;
        let mut i = 0;
        while i < small.len() {
            // SAFETY: `i < small.len() <= big.len()`
            let (mut out_elem, new_carry) = unsafe { big.get_unchecked(i).overflowing_sub(*small.get_unchecked(i)) };
            if carry {
                (out_elem, carry) = out_elem.overflowing_sub(1);
            }
            // SAFETY: guaranteed by caller
            unsafe { output_slice.get_unchecked_mut(i) }.write(out_elem);
            carry |= new_carry;
            i += 1;
        }
        // SAFETY: Every element of `output` has been initialized
        let mut output = unsafe { output.assume_init() };
        while carry {
            // SAFETY:
            // `small < big`, therefore the carry will end while `i < out.len()`
            let out_elem = unsafe { output.get_unchecked_mut(i) };
            (*out_elem, carry) = out_elem.overflowing_sub(1);
            i += 1;
        }
        (output, overflow)
    }
}

impl MulInto for UintSlice {
    /// Multiplies `self` by `rhs` and places the result in `output`
    ///
    /// # Safety
    /// `output.len()` must be greater than or equal to `self.len() + other.len()`
    type MaybeUninitSlice = MaybeUninitUintSlice;
    #[must_use]
    unsafe fn mul_into<M>(&self, other: &Self, mut output: M) -> M::Initialized
    where
        M: MaybeUninitTrait + Deref<Target = Self::MaybeUninitSlice> + DerefMut,
        M::Initialized: Deref<Target = <Self::MaybeUninitSlice as MaybeUninitMut>::Initialized> + DerefMut
    {
        debug_assert!(output.len() >= crate::bits_to_len(self.bits() + other.bits()));
        output.iter_mut().for_each(|x| { x.write(0); });
        // SAFETY: by inspection
        let mut out = unsafe { output.assume_init() };
        // SAFETY: guaranteed by caller
        unsafe { self.mul_base(other, &mut *out) }
        out
    }
}

impl DivRemInto for UintSlice {
    type DivUninitSlice = MaybeUninitUintSlice;
    type RemUninitSlice = MaybeUninitUintSlice;
    /// Divides `self` by `other`, placing the result in `div` and the remainder
    /// in `rem`
    ///
    /// # Safety
    /// 1. `rem.len()` must be greater than or equal to `bits_to_len(
    /// other.bits() + HalfBigit::BITS as Shift)`
    /// 2. `div.len()` must be greater than or equal to `bits_to_len(
    /// self.bits() - other.bits() + 1)`
    ///
    /// # Panics
    /// Panics if `other` is zero
    #[must_use]
    unsafe fn div_rem_into<D, R>(&self, other: &Self, mut div: D, mut rem: R) -> (D::Initialized, R::Initialized)
    where
        D: MaybeUninitTrait + Deref<Target = Self::DivUninitSlice> + DerefMut,
        D::Initialized: Deref<Target = <Self::DivUninitSlice as MaybeUninitMut>::Initialized> + DerefMut,
        R: MaybeUninitTrait + Deref<Target = Self::RemUninitSlice> + DerefMut,
        R::Initialized: Deref<Target = <Self::RemUninitSlice as MaybeUninitMut>::Initialized> + DerefMut
    {
        let denominator = other.trim_zeros();
        debug_assert!(rem.len() >= crate::bits_to_len(denominator.bits() + HalfBigit::BITS as Shift));

        let numerator = self.trim_zeros();
        // Shortcut 1: numerator is smaller than denominator
        if numerator < denominator {
            div.iter_mut().for_each(|x| { x.write(0); });
            let mut i = 0;
            while i < denominator.len() {
                // SAFETY: guaranteed by caller
                unsafe { rem.get_unchecked_mut(i).write(*denominator.get_unchecked(i)) };
                i += 1;
            }
            while i < rem.len() {
                // SAFETY: guaranteed by caller
                unsafe { rem.get_unchecked_mut(i) }.write(0);
                i += 1;
            }
            // SAFETY: All elements of `div` and `rem` have been initialized
            return unsafe { (div.assume_init(), rem.assume_init()) }
        }

        // Shortcut 2: division by zero
        if denominator.len() == 0 {
            panic!("attempt to divide by zero")
        }
        debug_assert!(div.len() >= crate::bits_to_len(numerator.bits() - denominator.bits() + 1));

        // Shortcut 3: they're both constants
        if let [numerator_value] = numerator.0 {
            // SAFETY: This was just checked to be nonzero
            let denominator_value = unsafe { denominator.get_unchecked(0) };
            // SAFETY: Guaranteed by caller
            unsafe { div.get_unchecked_mut(0) }.write(denominator_value/numerator_value);
            // SAFETY: guaranteed by caller
            unsafe { rem.get_unchecked_mut(0) }.write(denominator_value%numerator_value);
            // SAFETY: guaranteed by caller
            unsafe { div.get_unchecked_mut(1..) }.iter_mut().for_each(|x| { x.write(0); });
            // SAFETY: guaranteed by caller
            unsafe { rem.get_unchecked_mut(1..) }.iter_mut().for_each(|x| { x.write(0); });
            // SAFETY: All elements of `div` and `rem` have been initialized
            return unsafe { (div.assume_init(), rem.assume_init()) }
        }

        let (denom_zeros, denom_top);
        match denominator.0 {
            // Shortcut 4: `other` fits into a HalfBigit, allowing one to
            // use constant division
            [only] if only <= HalfBigit::MAX as Bigit => {
                // SAFETY: guaranteed by caller
                div.iter_mut().zip(self).for_each(|(div_elem, self_elem)| { div_elem.write(*self_elem); });
                // SAFETY: guaranteed by caller
                unsafe { div.get_unchecked_mut(self.len()..) }.iter_mut().for_each(|x| { x.write(0); });
                // SAFETY: All elements of `div` have been initialized
                let mut div = unsafe { div.assume_init() };
                // SAFETY: guaranteed by caller
                unsafe { rem.get_unchecked_mut(0) }.write(div.div_rem_assign(only as HalfBigit) as Bigit);
                // SAFETY: guaranteed by caller
                unsafe { rem.get_unchecked_mut(1..) }.into_iter().for_each(|x| { x.write(0); });
                // SAFETY: All elements of `rem` have been initialized
                return (div, unsafe { rem.assume_init() })
            },

            // No more shortcuts remain, time to do real division
            [.., last] if last > (HalfBigit::MAX >> 1) as Bigit => {
                denom_zeros = last.leading_zeros();
                denom_top = last >> (HalfBigit::BITS - denom_zeros);
            },
            [.., second_last, last] => {
                denom_zeros = last.leading_zeros();
                denom_top = (second_last >> (Bigit::BITS + HalfBigit::BITS - denom_zeros))
                                | (last << (denom_zeros - HalfBigit::BITS));
            },

            // SAFETY:
            // Length zero was checked for at Shortcut 2.
            // Length one is covered by the first two cases.
            // All other lengths are covered by the last case.
            _ => unsafe { unreachable_unchecked() },
        };

        let denominator_bits = ((denominator.len() as Shift) << Bigit::BITS.trailing_zeros()) - denom_zeros as Shift;
        let rem_bits = denominator_bits + HalfBigit::BITS as Shift;
        // Safety: numerator is at least length 1 by shortcuts 1 & 2
        let numerator_bits = ((numerator.len() as Shift) << Bigit::BITS.trailing_zeros())
                                - unsafe { *numerator.get_unchecked(numerator.len() - 1) }.leading_zeros() as Shift;

        // Just set this slice to zero and xor the answer into it, it's faster
        // than trying to keep track of what's initialized and what's not.
        div.iter_mut().for_each(|x| { x.write(0); });
        // SAFETY: by inspection
        let mut div = unsafe { div.assume_init() };

        let mut rem_out;
        if rem_bits >= numerator_bits {
            // Just copy `numerator` into `rem` and go to the end
            let mut i = 0;
            while i < numerator.len() {
                // SAFETY: By inspection
                unsafe { rem.get_unchecked_mut(i).write(*numerator.get_unchecked(i)) };
                i += 1;
            }
            while i < rem.len() {
                // SAFETY: By inspection
                unsafe { rem.get_unchecked_mut(i).write(0) };
            }
            // SAFETY: All elements have been initialized
            rem_out = unsafe { rem.assume_init() }
        }
        else {
            // Divide a full HalfBigit worth of bits
            let mut out_loc = numerator_bits - rem_bits;
            let (mut out_major, mut out_minor) = crate::shift_to_parts(out_loc);

            rem_out = numerator.shr_into(out_loc, rem);

            let (rem_major, rem_minor) = crate::shift_to_parts(rem_bits);

            let (rem_slice, rem_last_ref);
            let mut rem_second_last_ref = MaybeUninit::uninit();
            let mut right_shift = MaybeUninit::uninit();
            if rem_minor == 0 {
                // SAFETY: guaranteed by caller
                rem_slice = unsafe { rem_out.get_unchecked_mut(..rem_major) };
                // SAFETY:
                // The length and allocation of rem_slice will not change until
                // after the function returns
                rem_last_ref = unsafe { rem_slice.0.as_ptr().add(rem_major - 1) };
            }
            else {
                rem_slice = unsafe { rem_out.get_unchecked_mut(..rem_major + 1) };
                // SAFETY:
                // The length and allocation of rem_slice will not change until
                // after the function returns
                rem_last_ref = unsafe { rem_slice.0.as_ptr().add(rem_major) };
                // SAFETY:
                // The length and allocation of rem_slice will not change until
                // after the function returns
                rem_second_last_ref.write(unsafe { rem_slice.0.as_ptr().add(rem_major - 1) });
                right_shift.write(Bigit::BITS - rem_minor);
            }

            loop {
                // SAFETY:
                // These pointers are designed to stay constant even while the
                // underlying data changes, taking operations out of the loop.
                let rem_top = unsafe { if rem_minor == 0 {
                    *rem_last_ref
                }
                else {
                    // SAFETY:
                    // These variables are only used when `rem_minor == 0`,
                    // which is the same condition they were initialized in.
                    (*rem_last_ref << rem_minor) | (*rem_second_last_ref.assume_init() >> right_shift.assume_init())
                } };

                // DIVISION!! 🤩
                let mut div_elem = rem_top / denom_top;

                // The new div_elem is set, now multiply it by denominator and
                // subtract from rem. The multiply-and-subtract is done as one
                // operation because this function is not allowed to allocate.
                let mut carry = 0;
                for (rem_elem, denom_elem) in rem_slice.iter_mut().zip(denominator.iter()) {
                    let mut add_to_carry;
                    (*rem_elem, add_to_carry) = rem_elem.overflowing_sub(carry);
                    let lower;
                    if div_elem > HalfBigit::MAX as Bigit { unsafe { unreachable_unchecked() } }
                    (lower, carry) = crate::widening_mul_half(div_elem as HalfBigit, *denom_elem);
                    if add_to_carry {
                        carry += 1;
                    }
                    (*rem_elem, add_to_carry) = rem_elem.overflowing_sub(lower);
                    if add_to_carry {
                        carry += 1;
                    }
                }

                // Because the division was done by a full-sized Bigit over a
                // full-sized HalfBigit, the output div_elem is at most two more
                // than the correct value.
                debug_assert!(carry <= 1, "{carry}");
                if carry > 0 {
                    // SAFETY: rem_slice and denominator are the same size
                    if unsafe { rem_slice.add_assign_slice(denominator) } {
                        div_elem -= 1;
                    }
                    else {
                        // SAFETY: rem_slice and denominator are the same size
                        let carry = unsafe { rem_slice.add_assign_slice(denominator) };
                        debug_assert!(carry);
                        div_elem -= 2;
                    }
                }

                // Place the new `div_elem` at the correct location in `div`
                let out_elem = unsafe { div.get_unchecked_mut(out_major) };
                *out_elem |= div_elem << out_minor;
                if out_minor > HalfBigit::BITS {
                    let out_elem = unsafe { div.get_unchecked_mut(out_major + 1) };
                    *out_elem |= div_elem >> (Bigit::BITS - out_minor);
                }

                // Shift `rem_slice` up, add new data from `div`, and prepare
                // for the next loop
                let current_rem_bits = rem_slice.bits();
                let next_out_loc = if current_rem_bits == 0 {
                    // SAFETY: by inspection
                    let mut next_num_top = unsafe { *numerator.get_unchecked(out_major) } & ((1 << out_minor) - 1);
                    while next_num_top == 0 && out_major > 0 {
                        out_major -= 1;
                        // SAFETY: by inspection
                        next_num_top = unsafe { *numerator.get_unchecked(out_major) };
                    }
                    let leading_zeros = next_num_top.leading_zeros();
                    out_loc = ((out_major as Shift + 1) << Bigit::BITS.trailing_zeros()) - (leading_zeros as Shift);
                    if out_loc <= rem_bits {
                        // Place final bits from `numerator` into `rem`
                        for i in 0..out_major {
                            let rem_ref = unsafe { rem_slice.get_unchecked_mut(i) };
                            *rem_ref = unsafe { *numerator.get_unchecked(i) };
                        }
                        let rem_ref = unsafe { rem_slice.get_unchecked_mut(out_major) };
                        *rem_ref = next_num_top;
                        if unsafe { rem_slice.get_unchecked(..=out_major) } < denominator {
                            return (div, rem_out)
                        }
                        break
                    }
                    out_minor = Bigit::BITS - leading_zeros;
                    out_loc - rem_bits
                }
                else {
                    let new_shift = rem_bits - current_rem_bits;
                    debug_assert!(new_shift >= HalfBigit::BITS as Shift, "{new_shift}");
                    if new_shift >= out_loc {
                        rem_slice.shr_assign(out_loc);
                        // Place final bits from `numerator` into `rem`
                        for i in 0..out_major {
                            let rem_ref = unsafe { rem_slice.get_unchecked_mut(i) };
                            *rem_ref = unsafe { *numerator.get_unchecked(i) };
                        }
                        let rem_ref = unsafe { rem_slice.get_unchecked_mut(out_major) };
                        *rem_ref |= unsafe { *numerator.get_unchecked(out_major) } & ((1 << out_minor) - 1);
                        if *rem_slice < denominator {
                            return (div, rem_out)
                        }
                        break
                    }
                    rem_slice.shr_assign(new_shift);
                    out_loc - new_shift
                };
                let (next_out_major, next_out_minor) = crate::shift_to_parts(next_out_loc);

                // Place new bits from `numerator` into `rem`
                todo!();

                // Housekeeping for next loop
                out_loc = next_out_loc;
                out_major = next_out_major;
                out_minor = next_out_minor;
            }
        }
        // Divide any remaining bits (HalfBigit::BITS or less)
        todo!()
    }
}

#[cfg(feature = "fmt")]
impl fmt::Binary for UintSlice {
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, elem) in self.iter().enumerate().rev() {
            if *elem == 0 {
                continue
            }
            if i == 0 {
                return fmt::Binary::fmt(elem, f)
            }
            let num_width = ((i + 1) << Bigit::BITS.trailing_zeros()) - elem.leading_zeros() as usize;
            let right_pad = super::numeric_prefix(f, num_width)?;
            write!(f, "{elem:b}")?;
            for elem in unsafe { self.get_unchecked(..i).iter().rev() } {
                write!(f, "{elem:0width$b}", width=Bigit::BITS as usize)?;
            }
            return super::add_padding(f, right_pad)
        }
        fmt::Binary::fmt(&0, f)
    }
}

#[cfg(feature = "fmt")]
impl fmt::Octal for UintSlice {
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (mut i, mut elem) in self.iter().enumerate().rev() {
            if *elem == 0 {
                continue
            }
            if i == 0 {
                return fmt::Octal::fmt(elem, f)
            }
            let bits = ((i as Bigit + 1) << Bigit::BITS.trailing_zeros()) - elem.leading_zeros() as Bigit;
            let mut num_width = (bits/3) as usize;
            if bits%3 > 0 {
                num_width += 1;
            }
            let right_pad = super::numeric_prefix(f, num_width)?;
            const BIT_DIFF: u32 = Bigit::BITS % 3;
            let mut shift = (i % 3) as u32;
            if BIT_DIFF == 1 && shift != 0 {
                shift ^= 3; // 1 -> 2, 2 -> 1
            }
            let out_elem = elem >> shift;
            if out_elem != 0 {
                write!(f, "{:o}", out_elem)?;
            }
            const WIDTH: usize = (Bigit::BITS/3) as usize;
            loop {
                let mut carry = elem & ((1 << shift) - 1);
                i -= 1;
                elem = unsafe { self.get_unchecked(i) };
                if shift == 0 {
                    write!(f, "{:0WIDTH$o}", elem >> BIT_DIFF)?;
                    shift = BIT_DIFF;
                }
                else if BIT_DIFF == 1 && shift == 1 {
                    write!(f, "{:0WIDTH$o}", (carry << (Bigit::BITS - 2) | (elem >> 2)))?;
                    shift = 2;
                }
                else {
                    let intermediate_shift = shift ^ 3;
                    carry <<= intermediate_shift;
                    write!(f, "{:o}", carry | (elem >> (Bigit::BITS - intermediate_shift)))?;
                    shift = (shift + BIT_DIFF) % 3;
                    write!(f, "{:0WIDTH$o}", (elem >> shift) & (Bigit::MAX >> BIT_DIFF))?;
                }
                if i == 0 {
                    return super::add_padding(f, right_pad)
                }
            }
        }
        fmt::Octal::fmt(&0, f)
    }
}

#[cfg(feature = "fmt")]
impl fmt::LowerHex for UintSlice {
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, elem) in self.iter().enumerate().rev() {
            if *elem == 0 {
                continue
            }
            if i == 0 {
                return fmt::LowerHex::fmt(elem, f)
            }
            let num_width = ((i + 1) << (Bigit::BITS.trailing_zeros() - 2)) - (elem.leading_zeros() >> 2) as usize;
            let right_pad = super::numeric_prefix(f, num_width)?;
            write!(f, "{elem:x}")?;
            for elem in unsafe { self.get_unchecked(..i).iter().rev() } {
                write!(f, "{elem:0width$x}", width=Bigit::BITS as usize / 4)?;
            }
            return super::add_padding(f, right_pad)
        }
        fmt::LowerHex::fmt(&0, f)
    }
}

#[cfg(feature = "fmt")]
impl fmt::UpperHex for UintSlice {
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (i, elem) in self.iter().enumerate().rev() {
            if *elem == 0 {
                continue
            }
            if i == 0 {
                return fmt::UpperHex::fmt(elem, f)
            }
            let num_width = ((i + 1) << (Bigit::BITS.trailing_zeros() - 2)) - (elem.leading_zeros() >> 2) as usize;
            let right_pad = super::numeric_prefix(f, num_width)?;
            write!(f, "{elem:X}")?;
            for elem in unsafe { self.get_unchecked(..i).iter().rev() } {
                write!(f, "{elem:0width$X}", width=Bigit::BITS as usize / 4)?;
            }
            return super::add_padding(f, right_pad)
        }
        fmt::UpperHex::fmt(&0, f)
    }
}

/// Turns a big-endian slice of [`Bigit`]s into a vector of `Display`-ready
/// components. The last element of this vector is returned separately for
/// convenience. This function allocates.
///
/// The separately-returned element is guaranteed to be greater than 0. It and
/// every other element is guaranteed to be less than [`HALF_BIGIT_TEN`].
///
/// # Safety
/// 1. `last_elem` must be a reference to the last element of `slice`
/// 2. `last_elem` must be nonzero
#[cfg(all(feature = "alloc", feature = "fmt"))]
#[must_use]
unsafe fn generate_digits(slice: &[Bigit], last_elem: &Bigit) -> (Vec<HalfBigit>, HalfBigit) {
    // Clone self onto heap to enable mutability
    let mut div = UintVec::from(Vec::from(slice));

    // Generate output buffer
    let bits = ((div.len() as Bigit) << Bigit::BITS.trailing_zeros()) - last_elem.leading_zeros() as Bigit;
    let mut halfbigit_digits = vec![
        mem::MaybeUninit::uninit(); // Use malloc, no need for calloc.
        (bits as f64 * crate::DISPLAY_ALLOC_FACTOR) as usize + 1 // Estimated size requirement
    ];
    for digit_elem in halfbigit_digits.iter_mut() {
        digit_elem.write(div.div_rem_assign(HALF_BIGIT_TEN));
        if let Some(0) = div.last() {
            // Safety: obvious
            div.pop();
        }
    }
    // Safety:
    // Every element of `halfbigit_digits` is initialized in the for loop above
    let mut halfbigit_digits: Vec<HalfBigit> = halfbigit_digits.into_iter().map(|elem|
        unsafe { elem.assume_init() }
    ).collect();

    let mut biggest_digit;
    // Remove top nonzero element. This loop should repeat at most once.
    loop {
        // SAFETY:
        // `halfbigit_digits` is guaranteed to contain at least one
        // nonzero element.
        biggest_digit = unsafe { halfbigit_digits.pop().unwrap_unchecked() };
        if biggest_digit != 0 {
            break
        }
    }
    (halfbigit_digits, biggest_digit)
}

#[cfg(all(feature = "alloc", feature = "fmt"))]
impl fmt::Display for UintSlice {
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut self_slice = &self.0;
        while let [smaller_slice @ .., elem] = self_slice {
            if *elem == 0 {
                self_slice = smaller_slice;
                continue
            }
            if smaller_slice.len() == 0 {
                return fmt::Display::fmt(elem, f);
            }

            // SAFETY:
            // 1. `elem` is a reference to the last element of `self_slice`
            //    due to its construction in the while let statement above
            // 2. `elem` is nonzero due the if statement above
            let (halfbigit_digits, biggest_digit) = unsafe { generate_digits(self_slice, elem) };

            // Get length of output
            let mut num_digits = halfbigit_digits.len() * HALF_BIGIT_DIGITS;
            let mut temp = biggest_digit;
            while temp > 0 {
                temp /= 10;
                num_digits += 1;
            }

            // Write buffer to formatter
            let right_pad = super::numeric_prefix(f, num_digits)?;
            write!(f, "{biggest_digit}")?;
            for big_digit in halfbigit_digits.iter().rev() {
                write!(f, "{big_digit:0HALF_BIGIT_DIGITS$}")?;
            }
            return super::add_padding(f, right_pad)
        }
        fmt::Display::fmt(&0, f)
    }
}

/// Writes big-endian slice `slice` to format `f` in exponential format, using
/// `exponent_char` to denote the start of the exponent.
///
/// # Safety
/// 1. `last_elem` must be a reference to the last element of `slice`
/// 1. `last_elem` must be nonzero
#[cfg(all(feature = "alloc", feature = "fmt"))]
#[must_use]
unsafe fn write_exp(f: &mut fmt::Formatter, slice: &[Bigit], last_elem: &Bigit, exponent_char: char) -> fmt::Result {
    // Safety guaranteed by the safety of this function
    let (mut halfbigit_digits, mut biggest_digit) = unsafe { generate_digits(slice, last_elem) };
    let mut halfbigit_digits = halfbigit_digits.as_mut_slice();

    let mut biggest_digit_precision = 0;
    let mut biggest_digit_ten = 10;
    while biggest_digit_ten <= biggest_digit {
        biggest_digit_precision += 1;
        biggest_digit_ten *= 10;
    }
    let mut exponent = halfbigit_digits.len() as u128 * HALF_BIGIT_DIGITS as u128 + biggest_digit_precision as u128;

    let (mut precision, smallest_digit);
    if let Some(value) = f.precision() {
        // Known precision, find cutoff and round
        precision = value;
        if precision < biggest_digit_precision { // Cutoff is within `biggest_digit`
            let diff = biggest_digit_precision - precision;
            let diff_ten = (10 as HalfBigit).pow(diff as u32);
            let round_up = biggest_digit % diff_ten >= diff_ten >> 1;
            biggest_digit /= diff_ten;
            if round_up {
                biggest_digit += 1;
                if biggest_digit == biggest_digit_ten/diff_ten {
                    precision += 1;
                    exponent += 1;
                }
            }
            biggest_digit_precision = precision;
            halfbigit_digits = &mut [];
            smallest_digit = None;
        }
        else if precision == biggest_digit_precision { // Cutoff is at end of `biggest_digit`
            if let [.., value] = halfbigit_digits {
                if *value >= HALF_BIGIT_TEN >> 1 { // Rounding up
                    biggest_digit += 1;
                    if biggest_digit == biggest_digit_ten {
                        biggest_digit_precision += 1;
                        precision += 1;
                        exponent += 1;
                    }
                }
            }
            halfbigit_digits = &mut [];
            smallest_digit = None;
        }
        else if (precision as u128) < exponent { // Cutoff is within `halfbigit_digits`
            // SAFETY:
            // `biggest_digit_precision < precision < exponent`, therefore
            // `halfbigit_digits` contains at least one element.
            let remaining_precision = precision - biggest_digit_precision;
            // SAFETY:
            // `precision < `exponent` therefore
            // `remaining_precision/HALF_BIGIT_DIGITS < halfbigit_digits.len()`
            let halfbigit_cutoff = halfbigit_digits.len() - remaining_precision/HALF_BIGIT_DIGITS;
            let last_elem_precision = remaining_precision%HALF_BIGIT_DIGITS;
            let last_elem_ten = (10 as HalfBigit).pow((HALF_BIGIT_DIGITS - last_elem_precision) as u32);
            // SAFETY: obvious
            let last_elem = unsafe { halfbigit_digits.get_unchecked(halfbigit_cutoff - 1) };
            let mut last_elem_div = *last_elem / last_elem_ten;
            let last_elem_rem = *last_elem % last_elem_ten;
            // SAFETY: obvious
            halfbigit_digits = unsafe { halfbigit_digits.get_unchecked_mut(halfbigit_cutoff..) };
            if last_elem_rem >= last_elem_ten >> 1 { // Rounding up
                last_elem_div += 1;
                if last_elem_div == HALF_BIGIT_TEN/last_elem_ten { // Carrying
                    last_elem_div = 0;
                    let mut carry = true;
                    for carried_elem in halfbigit_digits.iter_mut() {
                        if *carried_elem == HALF_BIGIT_TEN - 1 {
                            *carried_elem = 0;
                        }
                        else {
                            *carried_elem += 1;
                            carry = false;
                            break
                        }
                    }
                    if carry { // Carry extends into `biggest_digit`
                        biggest_digit += 1;
                        if biggest_digit == biggest_digit_ten {
                            biggest_digit_precision += 1;
                            precision += 1;
                            exponent += 1;
                        }
                    }
                }
            }
            smallest_digit = if last_elem_precision > 0 { Some((last_elem_div, last_elem_precision)) } else { None };
        }
        else if (precision as u128) == exponent { // Cutoff is at end of `halfbigit_digits`
            smallest_digit = None;
        }
        else { // Cutoff is after end of `halfbigit_digits`
            smallest_digit = Some((0, precision - exponent as usize));
        }
    }
    else {
        // Undetermined precision, merely remove all zeros from the end
        while let [0, smaller_slice @ ..] = halfbigit_digits {
            halfbigit_digits = smaller_slice;
        }
        if let [value, smaller_slice @ ..] = halfbigit_digits {
            halfbigit_digits = smaller_slice;
            let mut value = *value;
            let mut width = HALF_BIGIT_DIGITS;
            while value % 10 == 0 {
                value /= 10;
                width -= 1;
            }
            precision = biggest_digit_precision + halfbigit_digits.len() * HALF_BIGIT_DIGITS + width;
            smallest_digit = Some((value, width));
        }
        else {
            while biggest_digit % 10 == 0 {
                biggest_digit /= 10;
                biggest_digit_precision -= 1;
            }
            precision = biggest_digit_precision;
            smallest_digit = None;
        }
    }

    let mut exponent_ten = 10;
    let mut exponent_digits = 1;
    while exponent_ten <= exponent {
        exponent_ten *= 10;
        exponent_digits += 1;
    }

    // Inner str = first_digit + '.' + digits[..precision] + 'e' + exponent
    let mut inner_len = exponent_digits + 2; // First digit, 'e', exponent
    if precision > 0 {
        inner_len += precision + 1; // '.', other digits
    }
    // Write initial padding
    let right_pad = super::numeric_prefix(f, inner_len)?;

    let biggest_digit_factor = (10 as HalfBigit).pow(biggest_digit_precision as u32);
    write!(f, "{}", biggest_digit/biggest_digit_factor)?;
    if precision > 0 {
        f.write_char('.')?;
        if biggest_digit_factor > 1 {
            write!(f, "{:0biggest_digit_precision$}", biggest_digit%biggest_digit_factor)?;
        }
    }
    for big_digit in halfbigit_digits.iter().rev() {
        write!(f, "{big_digit:0HALF_BIGIT_DIGITS$}")?;
    }
    if let Some((digit, width)) = smallest_digit {
        write!(f, "{digit:0width$}")?;
    }
    f.write_char(exponent_char)?;
    write!(f, "{}", exponent)?;
    return super::add_padding(f, right_pad)
}

#[cfg(all(feature = "alloc", feature = "fmt"))]
impl fmt::LowerExp for UintSlice {
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut self_slice = &self.0;
        while let [smaller_slice @ .., elem] = self_slice {
            if *elem == 0 {
                self_slice = smaller_slice;
                continue
            }
            if smaller_slice.len() == 0 {
                return fmt::LowerExp::fmt(elem, f);
            }

            // SAFETY:
            // 1. `elem` is a reference to the last element of `self_slice`
            //    due to its construction in the while let statement above
            // 2. `elem` is nonzero due the if statement above
            return unsafe { write_exp(f, self_slice, elem, 'e') }
        }
        fmt::LowerExp::fmt(&0, f)
    }
}

#[cfg(all(feature = "alloc", feature = "fmt"))]
impl fmt::UpperExp for UintSlice {
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut self_slice = &self.0;
        while let [smaller_slice @ .., elem] = self_slice {
            if *elem == 0 {
                self_slice = smaller_slice;
                continue
            }
            if smaller_slice.len() == 0 {
                return fmt::UpperExp::fmt(elem, f);
            }

            // SAFETY:
            // 1. `elem` is a reference to the last element of `self_slice`
            //    due to its construction in the while let statement above
            // 2. `elem` is nonzero due the if statement above
            return unsafe { write_exp(f, self_slice, elem, 'E') }
        }
        fmt::UpperExp::fmt(&0, f)
    }
}

/* UTILITY IMPLS */
impl Hash for UintSlice {
    #[inline]
    fn hash<H: core::hash::Hasher>(&self, state: &mut H) {
        self.trim_zeros().0.hash(state)
    }
}

impl Eq for UintSlice {}

impl PartialEq for UintSlice {
    #[inline(always)]
    #[must_use]
    fn eq(&self, other: &Self) -> bool {
        self.cmp(other) == Ordering::Equal
    }
}

impl<U: Deref<Target = UintSlice>> PartialEq<U> for UintSlice {
    #[inline(always)]
    #[must_use]
    fn eq(&self, other: &U) -> bool {
        self.cmp(other.deref()) == Ordering::Equal
    }
}

impl PartialOrd for UintSlice {
    #[inline(always)]
    #[must_use]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<U: Deref<Target = UintSlice>> PartialOrd<U> for UintSlice {
    #[inline(always)]
    #[must_use]
    fn partial_cmp(&self, other: &U) -> Option<Ordering> {
        Some(self.cmp(other.deref()))
    }
}

impl<'a> From<&'a [Bigit]> for &'a UintSlice {
    #[inline(always)]
    #[must_use]
    fn from(value: &'a [Bigit]) -> Self {
        UintSlice::from_slice(value)
    }
}

impl<'a> From<&'a mut [Bigit]> for &'a mut UintSlice {
    #[inline(always)]
    #[must_use]
    fn from(slice: &'a mut [Bigit]) -> Self {
        // SAFETY:
        // - the input and output types are equivalent, [Bigit]
        // - type safety is ensured by the return value of the function
        // - the input and output pointers are both mutable, and have the same
        //   lifetime
        // - repr(transparent) ensures the types have the same layout
        unsafe { mem::transmute(slice) }
    }
}

macro_rules! slice_index_impl { ($slicetype:ty, $ty:ty) => {
    impl Index<$ty> for $slicetype {
        type Output = $slicetype;
        #[inline(always)]
        #[must_use]
        fn index(&self, index: $ty) -> &Self::Output {
            <&$slicetype>::from(self.0.index(index))
        }
    }

    impl IndexMut<$ty> for $slicetype {
        #[inline(always)]
        #[must_use]
        fn index_mut(&mut self, index: $ty) -> &mut Self::Output {
            <&mut $slicetype>::from(self.0.index_mut(index))
        }
    }

    impl Get<$ty> for $slicetype {
        #[inline(always)]
        #[must_use]
        fn get(&self, index: $ty) -> Option<&Self::Output> {
            match self.0.get(index) {
                Some(slice) => Some(<&$slicetype>::from(slice)),
                None => None,
            }
        }

        #[inline(always)]
        #[must_use]
        unsafe fn get_unchecked(&self, index: $ty) -> &Self::Output {
            // SAFETY: guaranteed by caller
            <&$slicetype>::from(unsafe { self.0.get_unchecked(index) })
        }
    }

    impl GetMut<$ty> for $slicetype {
        #[inline(always)]
        #[must_use]
        fn get_mut(&mut self, index: $ty) -> Option<&mut Self::Output> {
            match self.0.get_mut(index) {
                Some(slice) => Some(<&mut $slicetype>::from(slice)),
                None => None,
            }
        }

        #[inline(always)]
        #[must_use]
        unsafe fn get_unchecked_mut(&mut self, index: $ty) -> &mut Self::Output {
            // SAFETY: guaranteed by caller
            <&mut $slicetype>::from(unsafe { self.0.get_unchecked_mut(index) })
        }
    }
} }

macro_rules! slice_impl { ($slicetype:ty, $output:ty) => {
    slice_index_impl!($slicetype, (Bound<usize>, Bound<usize>));
    slice_index_impl!($slicetype, Range<usize>);
    slice_index_impl!($slicetype, RangeFull);
    slice_index_impl!($slicetype, RangeFrom<usize>);
    slice_index_impl!($slicetype, RangeInclusive<usize>);
    slice_index_impl!($slicetype, RangeTo<usize>);
    slice_index_impl!($slicetype, RangeToInclusive<usize>);

    impl Index<usize> for $slicetype {
        type Output = $output;
        #[inline(always)]
        #[must_use]
        fn index(&self, index: usize) -> &Self::Output {
            self.0.index(index)
        }
    }

    impl IndexMut<usize> for $slicetype {
        #[inline(always)]
        #[must_use]
        fn index_mut(&mut self, index: usize) -> &mut Self::Output {
            self.0.index_mut(index)
        }
    }

    impl<'a> IntoIterator for &'a $slicetype {
        type Item = &'a $output;
        type IntoIter = core::slice::Iter<'a, $output>;
        #[inline(always)]
        #[must_use]
        fn into_iter(self) -> Self::IntoIter {
            self.0.iter()
        }
    }

    impl<'a> IntoIterator for &'a mut $slicetype {
        type Item = &'a mut $output;
        type IntoIter = core::slice::IterMut<'a, $output>;
        #[inline(always)]
        #[must_use]
        fn into_iter(self) -> Self::IntoIter {
            self.0.iter_mut()
        }
    }

    impl Get<usize> for $slicetype {
        #[inline(always)]
        #[must_use]
        fn get(&self, index: usize) -> Option<&Self::Output> {
            self.0.get(index)
        }

        #[inline(always)]
        #[must_use]
        unsafe fn get_unchecked(&self, index: usize) -> &Self::Output {
            // SAFETY: guaranteed by caller
            unsafe { self.0.get_unchecked(index) }
        }
    }

    impl GetMut<usize> for $slicetype {
        #[inline(always)]
        #[must_use]
        fn get_mut(&mut self, index: usize) -> Option<&mut Self::Output> {
            self.0.get_mut(index)
        }

        #[inline(always)]
        #[must_use]
        unsafe fn get_unchecked_mut(&mut self, index: usize) -> &mut Self::Output {
            // SAFETY: guaranteed by caller
            unsafe { self.0.get_unchecked_mut(index) }
        }
    }
} }

slice_impl!(UintSlice, Bigit);
slice_impl!(MaybeUninitUintSlice, MaybeUninit<Bigit>);

#[cfg(test)]
mod tests {
    extern crate std;

    use std::collections::hash_map::DefaultHasher;
    use std::hash::{Hash, Hasher};
    #[cfg(all(feature = "alloc", feature = "fmt"))]
    use std::{format, vec};
    #[cfg(all(feature = "alloc", feature = "fmt"))]
    use std::string::{String, ToString};

    #[cfg(all(feature = "alloc", feature = "fmt"))]
    use lazy_static::lazy_static;

    #[cfg(all(feature = "alloc", feature = "fmt"))]
    use regex::{Captures, Regex};

    use crate::Bigit;
    #[cfg(all(feature = "alloc", feature = "fmt"))]
    use crate::{UintVec, BIGIT_DIGITS, BIGIT_TEN, HALF_BIGIT_DIGITS, HALF_BIGIT_TEN};

    use super::*;

    #[test]
    fn bits() {
        assert_eq!(UintArray::from([0]).bits(), 0);
        assert_eq!(UintArray::from([1]).bits(), 1);
        assert_eq!(UintArray::from([3]).bits(), 2);
        assert_eq!(UintArray::from([0, 1]).bits(), Bigit::BITS as Shift + 1);
        assert_eq!(UintArray::from([0, 0, 1]).bits(), Bigit::BITS as Shift * 2 + 1);
    }

    #[test]
    fn bits_to_len() {
        for slice in [
            &*UintArray::from([]),
            &*UintArray::from([0]),
            &*UintArray::from([1]),
            &*UintArray::from([3, 0]),
            &*UintArray::from([Bigit::MAX]),
            &*UintArray::from([Bigit::MAX, 0, 0]),
            &*UintArray::from([0, 0, 1]),
        ] {
            assert_eq!(crate::bits_to_len(slice.bits()), slice.trim_zeros().len(), "{slice:?}");
        }
    }

    #[test]
    fn trim_zeros() {
        assert_eq!(&UintArray::from([]).trim_zeros().0, &[]);
        assert_eq!(&UintArray::from([1]).trim_zeros().0, &[1]);
        assert_eq!(&UintArray::from([1, 0]).trim_zeros().0, &[1]);
        assert_eq!(&UintArray::from([0, 1]).trim_zeros().0, &[0, 1]);
        assert_eq!(&UintArray::from([1, 0, 0]).trim_zeros().0, &[1]);
        assert_eq!(&UintArray::from([0, 1, 0, 0]).trim_zeros().0, &[0, 1]);
    }

    #[test]
    fn trim_zeros_mut() {
        assert_eq!(&UintArray::from([]).trim_zeros_mut().0, &[]);
        assert_eq!(&UintArray::from([1]).trim_zeros_mut().0, &[1]);
        assert_eq!(&UintArray::from([1, 0]).trim_zeros_mut().0, &[1]);
        assert_eq!(&UintArray::from([0, 1]).trim_zeros_mut().0, &[0, 1]);
        assert_eq!(&UintArray::from([1, 0, 0]).trim_zeros_mut().0, &[1]);
        assert_eq!(&UintArray::from([0, 1, 0, 0]).trim_zeros_mut().0, &[0, 1]);
    }

    #[test]
    fn sub_len() {
        assert_eq!(UintArray::from([]).sub_len(&*UintArray::from([])), 0);
        assert_eq!(UintArray::from([]).sub_len(&*UintArray::from([0])), 0);
        assert_eq!(UintArray::from([1]).sub_len(&*UintArray::from([1])), 0);
        assert_eq!(UintArray::from([1]).sub_len(&*UintArray::from([1, 0])), 0);
        assert_eq!(UintArray::from([1, 3]).sub_len(&*UintArray::from([1, 3])), 0);
        assert_eq!(UintArray::from([2, 3]).sub_len(&*UintArray::from([1, 3])), 1);
        assert_eq!(UintArray::from([2, 3]).sub_len(&*UintArray::from([1, 3, 0])), 1);
        assert_eq!(UintArray::from([2, 0]).sub_len(&*UintArray::from([1, 3])), 2);
        assert_eq!(UintArray::from([2, 3]).sub_len(&*UintArray::from([1, 0])), 2);
        assert_eq!(UintArray::from([1, 4]).sub_len(&*UintArray::from([1, 3])), 2);
        assert_eq!(UintArray::from([1, 3]).sub_len(&*UintArray::from([1, 4])), 2);
        assert_eq!(UintArray::from([4, 3, 3]).sub_len(&*UintArray::from([5, 2, 3])), 2);
        assert_eq!(UintArray::from([4, 3]).sub_len(&*UintArray::from([5, 2, 3])), 3);
    }

    #[test]
    fn add_assign() {
        let mut val = UintArray::from([]);
        assert_eq!(val.add_assign(3), 3);

        let mut val = UintArray::from([Bigit::MAX - 1]);
        assert_eq!(val.add_assign(1), 0);
        assert_eq!(val.0, [Bigit::MAX]);

        assert_eq!(val.add_assign(Bigit::MAX), 1);
        assert_eq!(val.0, [Bigit::MAX - 1]);
    }

    #[test]
    fn add_assign_slice() {
        let mut val = UintArray::from([Bigit::MAX - 1]);
        assert_eq!(unsafe { val.add_assign_slice(&*UintArray::from([1])) }, false);
        assert_eq!(val.0, [Bigit::MAX]);

        assert_eq!(unsafe { val.add_assign_slice(&*UintArray::from([Bigit::MAX])) }, true);
        assert_eq!(val.0, [Bigit::MAX - 1]);

        let mut val = UintArray::from([Bigit::MAX - 1, 0]);
        assert_eq!(unsafe { val.add_assign_slice(&*UintArray::from([4])) }, false);
        assert_eq!(val.0, [2, 1]);

        assert_eq!(unsafe { val.add_assign_slice(&*UintArray::from([Bigit::MAX, Bigit::MAX - 2])) }, false);
        assert_eq!(val.0, [1, Bigit::MAX]);

        assert_eq!(unsafe { val.add_assign_slice(&*UintArray::from([Bigit::MAX])) }, true);
        assert_eq!(val.0, [0, 0]);
    }

    #[test]
    fn sub_assign_slice() {
        let mut num = UintArray::from([0, 0, 0]);

        assert_eq!(unsafe { num.sub_assign_slice(&*UintArray::from([])) }, false);
        assert_eq!(num, UintArray::from([0, 0, 0]));

        assert_eq!(unsafe { num.sub_assign_slice(&*UintArray::from([1])) }, true);
        assert_eq!(num, UintArray::from([1, 0, 0]));

        assert_eq!(unsafe { num.sub_assign_slice(&*UintArray::from([0, 1])) }, true);
        assert_eq!(num, UintArray::from([Bigit::MAX, 0, 0]));

        assert_eq!(unsafe { num.sub_assign_slice(&*UintArray::from([0, 2])) }, true);
        assert_eq!(num, UintArray::from([1, 1, 0]));

        assert_eq!(unsafe { num.sub_assign_slice(&*UintArray::from([2])) }, false);
        assert_eq!(num, UintArray::from([Bigit::MAX, 0, 0]));

        assert_eq!(unsafe { num.sub_assign_slice(&*UintArray::from([Bigit::MAX, 0, 1])) }, true);
        assert_eq!(num, UintArray::from([0, 0, 1]));

        assert_eq!(unsafe { num.sub_assign_slice(&*UintArray::from([Bigit::MAX, Bigit::MAX, 0])) }, false);
        assert_eq!(num, UintArray::from([1, 0, 0]));
    }

    #[test]
    fn mul_assign() {
        let mut num = UintArray::from([3 << HalfBigit::BITS, HalfBigit::MAX as Bigit + 2]);
        assert_eq!(num.mul_assign(HalfBigit::MAX), 1);
        assert_eq!(num.0, [(Bigit::MAX - 2) << HalfBigit::BITS, 1]);

        assert_eq!(num.mul_assign(0), 0);
        assert_eq!(num.0, [0, 0]);
    }

    #[test]
    fn mul_assign_bigit() {
        let mut num = UintArray::from([3, 1]);
        assert_eq!(num.mul_assign_bigit(Bigit::MAX), 1);
        assert_eq!(num.0, [Bigit::MAX - 2, 1]);

        let mut num = UintArray::from([Bigit::MAX; 3]);
        assert_eq!(num.mul_assign_bigit(Bigit::MAX), Bigit::MAX - 1);
        assert_eq!(num.0, [1, Bigit::MAX, Bigit::MAX]);

        assert_eq!(num.mul_assign_bigit(0), 0);
        assert_eq!(num.0, [0, 0, 0]);
    }

    #[test]
    #[should_panic]
    fn div_rem_assign_zero() {
        UintArray::from([7]).div_rem_assign(0);
    }

    #[test]
    fn div_rem_assign() {
        let mut num = UintArray::from([]);
        assert_eq!(num.div_rem_assign(3), 0);
        assert_eq!(num.0, []);

        let mut num = UintArray::from([7]);
        assert_eq!(num.div_rem_assign(3), 1);
        assert_eq!(num.0, [2]);

        let mut num = UintArray::from([7]);
        assert_eq!(num.div_rem_assign(10), 7);
        assert_eq!(num.0, [0]);

        let mut num = UintArray::from([7, 4]);
        assert_eq!(num.div_rem_assign(1), 0);
        assert_eq!(num.0, [7, 4]);

        assert_eq!(num.div_rem_assign(4), 3);
        assert_eq!(num.0, [1, 1]);

        let mut num = UintArray::from([7, 3]);
        assert_eq!(num.div_rem_assign(3), 1);
        assert_eq!(num.0, [2, 1]);

        let mut num = UintArray::from([(1 << HalfBigit::BITS) + 3, HalfBigit::MAX as Bigit - 1]);
        assert_eq!(num.div_rem_assign(HalfBigit::MAX), 3);
        assert_eq!(num.0, [Bigit::MAX << HalfBigit::BITS, 0]);
    }

    #[test]
    fn ord() {
        let none = UintArray::from([]);
        let zero = UintArray::from([0]);
        let one = UintArray::from([1]);
        let one_zero = UintArray::from([1, 0]);
        let two = UintArray::from([2]);
        let zero_one = UintArray::from([0, 1]);
        assert_eq!(none, none);
        assert_eq!(none, zero);
        assert_eq!(one, one);
        assert_eq!(one, one_zero);
        assert!(none < one);
        assert!(zero < one);
        assert!(none < one_zero);
        assert!(zero < one_zero);
        assert!(one < two);
        assert!(one_zero < two);
        assert!(two < zero_one);
        assert!(one > none);
        assert!(one > zero);
        assert!(one_zero > none);
        assert!(one_zero > zero);
        assert!(two > one);
        assert!(two > one_zero);
        assert!(zero_one > two);
    }

    #[test]
    fn shl_assign() {
        let mut num = UintArray::from([]);
        *num <<= 1;
        assert_eq!(num.0, []);

        let mut num = UintArray::from([Bigit::MAX, 0, 0]);
        *num <<= 0;
        assert_eq!(num.0, [Bigit::MAX, 0, 0]);

        *num <<= Bigit::BITS as Shift * 3;
        assert_eq!(num.0, [0, 0, 0]);

        let mut num = UintArray::from([Bigit::MAX, 0, 0]);
        *num <<= 1;
        assert_eq!(num.0, [Bigit::MAX << 1, 1, 0]);

        *num <<= Bigit::BITS as Shift;
        assert_eq!(num.0, [0, Bigit::MAX << 1, 1]);

        let mut num = UintArray::from([Bigit::MAX]);
        *num <<= Bigit::BITS as Shift + 1;
        assert_eq!(num.0, [0]);

        let mut num = UintArray::from([Bigit::MAX, 0]);
        *num <<= Bigit::BITS as Shift + 1;
        assert_eq!(num.0, [0, Bigit::MAX << 1]);

        let mut num = UintArray::from([Bigit::MAX, 0, 0]);
        *num <<= Bigit::BITS as Shift + 1;
        assert_eq!(num.0, [0, Bigit::MAX << 1, 1]);

        let mut num = UintArray::from([Bigit::MAX, 0, 0, 0]);
        *num <<= Bigit::BITS as Shift + 1;
        assert_eq!(num.0, [0, Bigit::MAX << 1, 1, 0]);
    }

    #[test]
    fn shr_assign() {
        let mut num = UintArray::from([]);
        *num >>= 1;
        assert_eq!(num.0, []);

        let mut num = UintArray::from([0, 0, Bigit::MAX]);
        *num >>= 0;
        assert_eq!(num.0, [0, 0, Bigit::MAX]);

        *num >>= Bigit::BITS as Shift - 1;
        assert_eq!(num.0, [0, Bigit::MAX << 1, 1]);

        *num >>= Bigit::BITS as Shift;
        assert_eq!(num.0, [Bigit::MAX << 1, 1, 0]);

        let mut num = UintArray::from([Bigit::MAX; 3]);
        *num >>= Bigit::BITS as Shift * 3;
        assert_eq!(num.0, [0, 0, 0]);

        let mut num = UintArray::from([Bigit::MAX; 3]);
        *num >>= Bigit::BITS as Shift * 3 + 1;
        assert_eq!(num.0, [0, 0, 0]);

        let mut num = UintArray::from([0, 0, Bigit::MAX]);
        *num >>= Bigit::BITS as Shift * 2 - 1;
        assert_eq!(num.0, [Bigit::MAX << 1, 1, 0]);
    }

    #[test]
    fn shl_into() {
        let zero = UintArray::from([]);
        let max = UintArray::from([Bigit::MAX]);

        assert_eq!(zero.shl_into(0, MaybeUninitUintArray::<0>::uninit()).0, []);
        assert_eq!(zero.shl_into(0, MaybeUninitUintArray::<3>::uninit()).0, [0, 0, 0]);
        assert_eq!(zero.shl_into(1, MaybeUninitUintArray::<3>::uninit()).0, [0, 0, 0]);
        assert_eq!(zero.shl_into(Bigit::BITS as Shift, MaybeUninitUintArray::<3>::uninit()).0, [0, 0, 0]);

        assert_eq!(max.shl_into(0, MaybeUninitUintArray::<0>::uninit()).0, []);
        assert_eq!(max.shl_into(0, MaybeUninitUintArray::<1>::uninit()).0, [Bigit::MAX]);
        assert_eq!(max.shl_into(0, MaybeUninitUintArray::<2>::uninit()).0, [Bigit::MAX, 0]);
        assert_eq!(max.shl_into(1, MaybeUninitUintArray::<0>::uninit()).0, []);
        assert_eq!(
            max.shl_into(1, MaybeUninitUintArray::<1>::uninit()),
            UintArray::from([Bigit::MAX << 1])
        );
        assert_eq!(
            max.shl_into(1, MaybeUninitUintArray::<2>::uninit()),
            UintArray::from([Bigit::MAX << 1, 1])
        );
        assert_eq!(
            max.shl_into(1, MaybeUninitUintArray::<3>::uninit()),
            UintArray::from([Bigit::MAX << 1, 1, 0])
        );
        assert_eq!(max.shl_into(Bigit::BITS as Shift, MaybeUninitUintArray::<0>::uninit()).0, []);
        assert_eq!(max.shl_into(Bigit::BITS as Shift, MaybeUninitUintArray::<1>::uninit()).0, [0]);
        assert_eq!(max.shl_into(Bigit::BITS as Shift, MaybeUninitUintArray::<2>::uninit()).0, [0, Bigit::MAX]);
        assert_eq!(max.shl_into(Bigit::BITS as Shift, MaybeUninitUintArray::<3>::uninit()).0, [0, Bigit::MAX, 0]);
        assert_eq!(
            max.shl_into(Bigit::BITS as Shift + 1, MaybeUninitUintArray::<1>::uninit()),
            UintArray::from([0])
        );
        assert_eq!(
            max.shl_into(Bigit::BITS as Shift + 1, MaybeUninitUintArray::<2>::uninit()),
            UintArray::from([0, Bigit::MAX << 1])
        );
        assert_eq!(
            max.shl_into(Bigit::BITS as Shift + 1, MaybeUninitUintArray::<3>::uninit()),
            UintArray::from([0, Bigit::MAX << 1, 1])
        );
        assert_eq!(
            max.shl_into(Bigit::BITS as Shift + 1, MaybeUninitUintArray::<4>::uninit()),
            UintArray::from([0, Bigit::MAX << 1, 1, 0])
        );
    }

    #[test]
    fn shr_into() {
        let zero = UintArray::from([]);
        let max = UintArray::from([0, 0, Bigit::MAX]);

        assert_eq!(zero.shr_into(0, MaybeUninitUintArray::<0>::uninit()).0, []);
        assert_eq!(zero.shr_into(0, MaybeUninitUintArray::<3>::uninit()).0, [0, 0, 0]);
        assert_eq!(zero.shr_into(1, MaybeUninitUintArray::<3>::uninit()).0, [0, 0, 0]);
        assert_eq!(zero.shr_into(Bigit::BITS as Shift, MaybeUninitUintArray::<3>::uninit()).0, [0, 0, 0]);

        assert_eq!(max.shr_into(1, MaybeUninitUintArray::<0>::uninit()).0, []);
        assert_eq!(max.shr_into(0, MaybeUninitUintArray::<3>::uninit()).0, [0, 0, Bigit::MAX]);
        assert_eq!(max.shr_into(0, MaybeUninitUintArray::<2>::uninit()).0, [0, 0]);
        assert_eq!(max.shr_into(Bigit::BITS as Shift, MaybeUninitUintArray::<1>::uninit()).0, [0]);
        assert_eq!(max.shr_into(Bigit::BITS as Shift, MaybeUninitUintArray::<2>::uninit()).0, [0, Bigit::MAX]);
        assert_eq!(max.shr_into(Bigit::BITS as Shift, MaybeUninitUintArray::<3>::uninit()).0, [0, Bigit::MAX, 0]);
        assert_eq!(max.shr_into(Bigit::BITS as Shift - 3, MaybeUninitUintArray::<2>::uninit()).0, [0, Bigit::MAX << 3]);
        assert_eq!(
            max.shr_into(Bigit::BITS as Shift + 3, MaybeUninitUintArray::<2>::uninit()).0,
            [Bigit::MAX << (Bigit::BITS - 3), Bigit::MAX >> 3]
        );
        assert_eq!(
            max.shr_into(Bigit::BITS as Shift * 2 + 3, MaybeUninitUintArray::<2>::uninit()).0, [Bigit::MAX >> 3, 0]
        );
        assert_eq!(max.shr_into(Bigit::BITS as Shift * 3, MaybeUninitUintArray::<2>::uninit()).0, [0, 0]);
        assert_eq!(max.shr_into(Bigit::BITS as Shift * 3 + 1, MaybeUninitUintArray::<2>::uninit()).0, [0, 0]);
    }

    #[test]
    fn add_into() {
        let one = UintArray::from([1]);
        assert_eq!(
            unsafe { one.add_into(&*UintArray::from([]), MaybeUninitUintArray::<2>::uninit()) },
            (UintArray::from([1, 0]), false)
        );
        assert_eq!(
            unsafe { one.add_into(&*UintArray::from([Bigit::MAX]), MaybeUninitUintArray::<2>::uninit()) },
            (UintArray::from([0, 1]), false)
        );
        assert_eq!(
            unsafe { one.add_into(&*UintArray::from([0, Bigit::MAX]), MaybeUninitUintArray::<2>::uninit()) },
            (UintArray::from([1, Bigit::MAX]), false)
        );
        assert_eq!(
            unsafe { one.add_into(&*UintArray::from([Bigit::MAX; 2]), MaybeUninitUintArray::<2>::uninit()) },
            (UintArray::from([0, 0]), true)
        );
        assert_eq!(
            unsafe { one.add_into(&*UintArray::from([Bigit::MAX; 2]), MaybeUninitUintArray::<3>::uninit()) },
            (UintArray::from([0, 0, 1]), false)
        );
    }

    #[test]
    fn sub_into() {
        assert_eq!(
            unsafe { UintArray::from([3]).sub_into(&*UintArray::from([2]), MaybeUninitUintArray::<1>::uninit()) },
            (UintArray::from([1]), false)
        );

        assert_eq!(
            unsafe { UintArray::from([2]).sub_into(&*UintArray::from([3]), MaybeUninitUintArray::<1>::uninit()) },
            (UintArray::from([1]), true)
        );

        assert_eq!(
            unsafe { UintArray::from([3]).sub_into(&*UintArray::from([3]), MaybeUninitUintArray::<0>::uninit()) },
            (UintArray::from([]), false)
        );

        assert_eq!(
            unsafe { UintArray::from([3]).sub_into(&*UintArray::from([]), MaybeUninitUintArray::<1>::uninit()) },
            (UintArray::from([3]), false)
        );

        assert_eq!(
            unsafe { UintArray::from([]).sub_into(&*UintArray::from([2]), MaybeUninitUintArray::<1>::uninit()) },
            (UintArray::from([2]), true)
        );

        assert_eq!(
            unsafe { UintArray::from([2, 1]).sub_into(&*UintArray::from([3, 1]), MaybeUninitUintArray::<1>::uninit()) },
            (UintArray::from([1]), true)
        );

        assert_eq!(
            unsafe {
                UintArray::from([1, 2, 3, 4, 5])
                .sub_into(&*UintArray::from([0, 0, 0, 4, 5]), MaybeUninitUintArray::<3>::uninit())
            },
            (UintArray::from([1, 2, 3]), false)
        );

        assert_eq!(
            unsafe {
                UintArray::from([1, 2, 3, 4, 0])
                .sub_into(&*UintArray([0, 0, 0, 4]), MaybeUninitUintArray::<3>::uninit())
            },
            (UintArray::from([1, 2, 3]), false)
        );

        assert_eq!(
            unsafe {
                UintArray::from([0, 0, 2])
                .sub_into(&*UintArray([Bigit::MAX, Bigit::MAX, 1]), MaybeUninitUintArray::<3>::uninit())
            },
            (UintArray::from([1, 0, 0]), false)
        );

        assert_eq!(
            unsafe {
                UintArray::from([Bigit::MAX, Bigit::MAX])
                .sub_into(&*UintArray([0, 0, 1]), MaybeUninitUintArray::<3>::uninit())
            },
            (UintArray::from([1, 0, 0]), true)
        );
    }

    #[test]
    fn mul_into() {
        let num = UintArray::from([Bigit::MAX; 2]);
        let result = unsafe { num.mul_into(&*UintArray::from([2]), MaybeUninitUintArray::<3>::uninit()) };
        assert_eq!(result, UintArray::from([Bigit::MAX << 1, Bigit::MAX, 1]));
        let result = unsafe { num.mul_into(&*num, MaybeUninitUintArray::<4>::uninit()) };
        assert_eq!(result, UintArray::from([1, 0, Bigit::MAX - 1, Bigit::MAX]));

        let num = UintArray::from([Bigit::MAX, 0, 2]);
        let result = unsafe { num.mul_into(&*UintArray::from([2]), MaybeUninitUintArray::<4>::uninit()) };
        assert_eq!(result, UintArray::from([Bigit::MAX - 1, 1, 4, 0]));
        let result = unsafe { num.mul_into(&*UintArray::from([Bigit::MAX]), MaybeUninitUintArray::<4>::uninit()) };
        assert_eq!(result, UintArray::from([1, Bigit::MAX - 1, Bigit::MAX - 1, 1]));
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn binary() {
        let zero = UintArray::from([0, 0]);
        assert_eq!(format!("{:b}", zero), format!("{:b}", 0));
        let small = UintArray::from([15, 0]);
        assert_eq!(format!("{:b}", small), format!("{:b}", 15));
        let big = UintArray::from([Bigit::MAX, 1, 0]);
        let normal = format!("1{:b}", Bigit::MAX);
        assert_eq!(format!("{:b}", big), format!("{normal}"));
        assert_eq!(format!("{:+b}", big), format!("+{normal}"));
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn octal_basic() {
        let zero = UintArray::from([0, 0]);
        assert_eq!(format!("{:o}", zero), format!("{:o}", 0));
        let small = UintArray::from([15, 0]);
        assert_eq!(format!("{:o}", small), format!("{:o}", 15));
        for top in [0, 1, 3, 7] {
            let mut big = vec![top, 0];
            for i in 0..3 {
                big.insert(0, Bigit::MAX);
                let big = <&UintSlice>::from(big.as_slice());
                let bits = big.bits();
                let extra_bits = bits%3;
                let normal = if extra_bits == 0 {
                    format!("{:7<width$o}", 7, width=(bits/3) as usize)
                }
                else {
                    format!("{:o}{:7<width$o}", (1 << extra_bits) - 1, 7, width=(bits/3) as usize)
                };
                assert_eq!(format!("{:o}", big), normal, "{top} {i}");
            }
        }
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn octal_boundaries() {
        let base = 1000000;
        for shift in 0..3 {
            let base = base << shift;
            let mut num = UintArray::from([base, 0, 0, 0]);
            let mut width = format!("{base:o}").len();
            for i in 0..Bigit::BITS as usize {
                assert_eq!(format!("{num:o}"), format!("{base:0<width$o}"), "{shift} {i}");
                width += 1;
                *num <<= 3;
            }
        }
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn lower_hex() {
        let zero = UintArray::from([0, 0]);
        assert_eq!(format!("{:x}", zero), format!("{:x}", 0));
        let small = UintArray::from([15, 0]);
        assert_eq!(format!("{:x}", small), format!("{:x}", 15));
        let big = UintArray::from([Bigit::MAX, 1, 0]);
        let normal = format!("1{:x}", Bigit::MAX);
        assert_eq!(format!("{:x}", big), normal);
        assert_eq!(format!("{:<width$x}", big, width=normal.len() + 1), format!("{normal} "));
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn upper_hex() {
        let zero = UintArray::from([0, 0]);
        assert_eq!(format!("{:X}", zero), format!("{:X}", 0));
        let small = UintArray::from([15, 0]);
        assert_eq!(format!("{:X}", small), format!("{:X}", 15));
        let big = UintArray::from([Bigit::MAX, 1, 0]);
        let normal = format!("1{:X}", Bigit::MAX);
        assert_eq!(format!("{:X}", big), normal);
        assert_eq!(format!("{:<width$X}", big, width=normal.len() + 1), format!("{normal} "));
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn hex_zeroed() {
        let big = UintArray::from([Bigit::MAX, 1, 0]);
        let normal = format!("1{:x}", Bigit::MAX);
        assert_eq!(format!("{:0width$x}", big, width=normal.len()), format!("{normal}"));
        assert_eq!(format!("{:0width$x}", big, width=normal.len() + 1), format!("0{normal}"));
        assert_eq!(format!("{:+0width$x}", big, width=normal.len() + 1), format!("+{normal}"));
        assert_eq!(format!("{:+0width$x}", big, width=normal.len() + 2), format!("+0{normal}"));
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn hex_unaligned() {
        let big = UintArray::from([Bigit::MAX, 1, 0]);
        let normal = format!("1{:x}", Bigit::MAX);
        assert_eq!(format!("{:width$x}", big, width=normal.len()), format!("{normal}"));
        assert_eq!(format!("{:width$x}", big, width=normal.len() + 1), format!(" {normal}"));
        assert_eq!(format!("{:+width$x}", big, width=normal.len() + 1), format!("+{normal}"));
        assert_eq!(format!("{:+width$x}", big, width=normal.len() + 2), format!(" +{normal}"));
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn hex_aligned_right() {
        let big = UintArray::from([Bigit::MAX, 1, 0]);
        let normal = format!("1{:x}", Bigit::MAX);
        assert_eq!(format!("{:width$x}", big, width=normal.len()), format!("{normal}"));
        assert_eq!(format!("{:width$x}", big, width=normal.len() + 1), format!(" {normal}"));
        assert_eq!(format!("{:>+width$x}", big, width=normal.len() + 1), format!("+{normal}"));
        assert_eq!(format!("{:>+width$x}", big, width=normal.len() + 2), format!(" +{normal}"));
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn hex_aligned_left() {
        let big = UintArray::from([Bigit::MAX, 1, 0]);
        let normal = format!("1{:x}", Bigit::MAX);
        assert_eq!(format!("{:<width$x}", big, width=normal.len()), format!("{normal}"));
        assert_eq!(format!("{:<width$x}", big, width=normal.len() + 1), format!("{normal} "));
        assert_eq!(format!("{:<+width$x}", big, width=normal.len() + 1), format!("+{normal}"));
        assert_eq!(format!("{:<+width$x}", big, width=normal.len() + 2), format!("+{normal} "));
    }

    #[cfg(feature = "fmt")]
    #[test]
    fn hex_aligned_center() {
        let big = UintArray::from([Bigit::MAX, 1, 0]);
        let normal = format!("1{:x}", Bigit::MAX);
        assert_eq!(format!("{:^width$x}", big, width=normal.len()), format!("{normal}"));
        assert_eq!(format!("{:^width$x}", big, width=normal.len() + 1), format!("{normal} "));
        assert_eq!(format!("{:^width$x}", big, width=normal.len() + 2), format!(" {normal} "));
        assert_eq!(format!("{:^width$x}", big, width=normal.len() + 3), format!(" {normal}  "));
        assert_eq!(format!("{:^+width$x}", big, width=normal.len() + 1), format!("+{normal}"));
        assert_eq!(format!("{:^+width$x}", big, width=normal.len() + 2), format!("+{normal} "));
        assert_eq!(format!("{:^+width$x}", big, width=normal.len() + 3), format!(" +{normal} "));
        assert_eq!(format!("{:^+width$x}", big, width=normal.len() + 4), format!(" +{normal}  "));
    }

    #[cfg(all(feature = "alloc", feature = "fmt"))]
    #[test]
    fn display() {
        let mut num = UintVec::from(vec![]);
        assert_eq!(format!("{num}"), "0");
        num = UintVec::from(vec![0, 0]);
        assert_eq!(format!("{num}"), "0");
        for base in [1, 4, 5, 9, 14, 15, 94, 95, BIGIT_TEN - 6, BIGIT_TEN - 5, BIGIT_TEN, Bigit::MAX] {
            num = UintVec::from(vec![base, 0, 0]);
            let mut width = format!("{base}").len();
            for i in 0..BIGIT_DIGITS*2 {
                assert_eq!(format!("{num}"), format!("{base:0<width$}"), "{i}");
                width += 1;
                num.mul_assign_halfbigit(10);
            }
        }
    }

    #[cfg(all(feature = "alloc", feature = "fmt"))]
    #[test]
    fn exponent() {
        let mut num = UintVec::from(vec![]);
        assert_eq!(format!("{num:E}"), format!("{:E}", 0));
        assert_eq!(format!("{num:e}"), format!("{:e}", 0));
        num = UintVec::from(vec![0, 0]);
        assert_eq!(format!("{num:E}"), format!("{:E}", 0));
        assert_eq!(format!("{num:e}"), format!("{:e}", 0));

        fn base_times_ten(base: String, exponent: usize) -> String {
            lazy_static!{
                static ref RE: Regex = Regex::new(r"^(.*[e|E])([0-9]+)$").unwrap();
            }
            (*RE.replace(&base, |cap: &Captures| {
                let old_len = cap.get(0).unwrap().as_str().len();
                let out = format!("{}{}",
                    cap.get(1).unwrap().as_str(),
                    cap.get(2).unwrap().as_str().parse::<usize>().unwrap() + exponent
                );
                if out.chars().next() == Some(' ') { out[out.len() - old_len..].to_string() } else { out }
            })).to_string()
        }
        for base in [
            1, 4, 5, 14, 15, 94, 95, 194, 195, 994, 995,
            HALF_BIGIT_TEN as Bigit - 6, HALF_BIGIT_TEN as Bigit - 5, HALF_BIGIT_TEN as Bigit,
            HALF_BIGIT_TEN as Bigit * 10 - 6, HALF_BIGIT_TEN as Bigit * 10 - 5, HALF_BIGIT_TEN as Bigit * 10,
            BIGIT_TEN - 6, BIGIT_TEN - 5, BIGIT_TEN, Bigit::MAX
        ] {
            num = UintVec::from(vec![base, 0, 0]);
            for i in 0..BIGIT_DIGITS*2 {
                assert_eq!(format!("{num:50E}"), base_times_ten(format!("{base:50E}"), i), "{base} {i}");
                assert_eq!(format!("{num:50e}"), base_times_ten(format!("{base:50e}"), i), "{base} {i}");
                assert_eq!(format!("{num:50.0e}"), base_times_ten(format!("{base:50.0e}"), i), "{base} {i}");
                assert_eq!(format!("{num:50.1e}"), base_times_ten(format!("{base:50.1e}"), i), "{base} {i}");
                assert_eq!(format!("{num:50.2e}"), base_times_ten(format!("{base:50.2e}"), i), "{base} {i}");
                assert_eq!(format!("{num:50.3e}"), base_times_ten(format!("{base:50.3e}"), i), "{base} {i}");
                assert_eq!(
                    format!("{num:50.precision$e}", precision=HALF_BIGIT_DIGITS - 1),
                    base_times_ten(format!("{base:50.precision$e}", precision=HALF_BIGIT_DIGITS - 1), i), "{base} {i}"
                );
                assert_eq!(
                    format!("{num:50.precision$e}", precision=HALF_BIGIT_DIGITS),
                    base_times_ten(format!("{base:50.precision$e}", precision=HALF_BIGIT_DIGITS), i), "{base} {i}"
                );
                assert_eq!(
                    format!("{num:50.precision$e}", precision=HALF_BIGIT_DIGITS + 1),
                    base_times_ten(format!("{base:50.precision$e}", precision=HALF_BIGIT_DIGITS + 1), i), "{base} {i}"
                );
                assert_eq!(
                    format!("{num:50.precision$e}", precision=BIGIT_DIGITS - 1),
                    base_times_ten(format!("{base:50.precision$e}", precision=BIGIT_DIGITS - 1), i), "{base} {i}"
                );
                assert_eq!(
                    format!("{num:50.precision$e}", precision=BIGIT_DIGITS),
                    base_times_ten(format!("{base:50.precision$e}", precision=BIGIT_DIGITS), i), "{base} {i}"
                );
                assert_eq!(
                    format!("{num:50.precision$e}", precision=BIGIT_DIGITS + 1),
                    base_times_ten(format!("{base:50.precision$e}", precision=BIGIT_DIGITS + 1), i), "{base} {i}"
                );
                num.mul_assign_halfbigit(10);
            }
        }
    }

    #[test]
    fn hash() {
        fn get_hash<const N: usize>(x: [Bigit; N]) -> u64 {
            let mut hasher = DefaultHasher::new();
            UintArray::from(x).hash(&mut hasher);
            hasher.finish()
        }
        assert_eq!(get_hash([]), get_hash([0]));
        assert_eq!(get_hash([0]), get_hash([0]));
        assert_eq!(get_hash([0]), get_hash([0, 0]));
        assert_eq!(get_hash([1]), get_hash([1]));
        assert_eq!(get_hash([1]), get_hash([1, 0]));
        assert_eq!(get_hash([1, 2, 3]), get_hash([1, 2, 3, 0]));
        assert_ne!(get_hash([]), get_hash([1]));
        assert_ne!(get_hash([1, 2, 3]), get_hash([0, 1, 2, 3]));
    }
}