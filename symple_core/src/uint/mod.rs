// Licensed under the& Apache License, Version 2.0 <LICENSE-APACHE or
// &http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Functions for operating on unsigned integer values. This module is the
//! backend for Symple's [`Natural`], [`Integer`], and [`Rational`] numbers.
//!
//! To start using this module, create a [`UintArray`] or [`UintVec`] using the
//! `From<Bigit>` impl. Both structs implement `Deref<Target = UintSlice>` and
//! `DerefMut`. This allows them to be used interchangeably in the core
//! algorithms, all of which are implemented on the [`UintSlice`] struct.
//!
//! Many [`slice`], [`array`], and [`Vec`] functions have not yet been
//! reimplemented for [`UintSlice`], [`UintArray`], and [`UintVec`]. If you
//! would use one of those functions, please
//! [`report it`](https://gitlab.com/symple1/symple-core/-/issues).
//!
//! [`Natural`]: https://docs.rs/symple/latest/symple/number/Natural
//! [`Integer`]: https://docs.rs/symple/latest/symple/number/Integer
//! [`Rational`]: https://docs.rs/symple/latest/symple/number/Rational
//!
//! [`slice`]: https://doc.rust-lang.org/stable/core/primitive.slice.html
//! [`array`]: https://doc.rust-lang.org/stable/core/primitive.array.html
//! [`Vec`]: https://doc.rust-lang.org/alloc/vec/struct.Vec.html

#[cfg(feature = "fmt")]
use core::fmt::{self, Alignment, Write};
use core::mem::MaybeUninit;

#[cfg(feature = "alloc")]
use alloc::vec::Vec;

use crate::Bigit;

mod array;
mod slice;
#[cfg(feature = "alloc")]
mod vec;

/// Slice containing a big unsigned integer.
///
/// This slice is "big-endian" -- the least significant [`Bigit`] is at index 0.
///
/// [`Integer`]: https://docs.rs/symple/latest/symple/number/Integer
#[cfg_attr(any(test, feature = "fmt"), derive(Debug))]
#[repr(transparent)]
pub struct UintSlice([Bigit]);

/// Big-endian slice of [`MaybeUninit<Bigit>`] values
#[cfg_attr(any(test, feature = "fmt"), derive(Debug))]
#[repr(transparent)]
pub struct MaybeUninitUintSlice([MaybeUninit<Bigit>]);

#[cfg_attr(any(test, feature = "fmt"), derive(Debug))]
#[repr(transparent)]
pub struct UintArray<const LEN: usize>([Bigit; LEN]);

#[cfg_attr(any(test, feature = "fmt"), derive(Debug))]
#[repr(transparent)]
pub struct MaybeUninitUintArray<const LEN: usize>([MaybeUninit<Bigit>; LEN]);

#[cfg(feature = "alloc")]
#[cfg_attr(any(test, feature = "fmt"), derive(Debug))]
#[repr(transparent)]
pub struct UintVec(Vec<Bigit>);

#[cfg(feature = "alloc")]
#[cfg_attr(any(test, feature = "fmt"), derive(Debug))]
#[repr(transparent)]
pub struct MaybeUninitUintVec(Vec<MaybeUninit<Bigit>>);

#[cfg(feature = "fmt")]
#[inline(always)]
#[must_use]
fn add_padding(f: &mut fmt::Formatter, count: usize) -> fmt::Result {
    for _ in 0..count {
        f.write_char(f.fill())?;
    }
    Ok(())
}

#[cfg(feature = "fmt")]
#[must_use]
fn numeric_prefix(f: &mut fmt::Formatter, mut num_width: usize) -> core::result::Result<usize, fmt::Error> {
    let mut right_pad = 0;
    if let Some(total_width) = f.width() {
        if f.sign_plus() {
            num_width += 1;
        }
        if total_width > num_width {
            let additional_width = total_width - num_width;
            if f.sign_aware_zero_pad() {
                if f.sign_plus() {
                    f.write_char('+')?;
                }
                for _ in 0..additional_width {
                    f.write_char('0')?;
                }
                return Ok(right_pad)
            }
            match f.align() {
                None | Some(Alignment::Right) => add_padding(f, additional_width)?,
                Some(Alignment::Left) => {
                    right_pad = additional_width;
                },
                Some(Alignment::Center) => {
                    let left_pad = additional_width >> 1;
                    add_padding(f, left_pad)?;
                    right_pad = additional_width - left_pad;
                },
            }
        }
    }
    if f.sign_plus() {
        f.write_char('+')?;
    }
    return Ok(right_pad);
}
