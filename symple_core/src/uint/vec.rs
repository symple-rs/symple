// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#[cfg(feature = "fmt")]
use core::fmt;
use core::hash::{Hash, Hasher};
use core::mem::MaybeUninit;
use core::ops::{
    Deref,
    DerefMut,
};

use alloc::vec;
use alloc::vec::Vec;

use crate::{Bigit, MaybeUninitTrait};
use crate::uint::{UintSlice, MaybeUninitUintSlice, UintVec, MaybeUninitUintVec};

impl MaybeUninitUintVec {
    #[inline(always)]
    #[must_use]
    pub fn uninit(len: usize) -> Self {
        Self(vec![MaybeUninit::uninit(); len])
    }
}

impl MaybeUninitTrait for MaybeUninitUintVec {
    type Initialized = UintVec;
    /// Extracts the value of the array from the inner `MaybeUninit` containers
    ///
    /// # Safety
    /// All elements of the array must be in an initialized state
    #[inline(always)]
    #[must_use]
    unsafe fn assume_init(mut self) -> Self::Initialized {
        // SAFETY: guaranteed by caller
        UintVec(self.0.iter_mut().map(|x| unsafe { x.assume_init() }).collect())
    }
}


impl UintVec {
    #[inline(always)]
    pub fn pop(&mut self) -> Option<Bigit> {
        self.0.pop()
    }
}

impl From<Bigit> for UintVec {
    #[inline(always)]
    #[must_use]
    fn from(value: Bigit) -> Self {
        UintVec(if value == 0 { Vec::new() } else { vec![value] })
    }
}

impl From<&UintSlice> for UintVec {
    #[inline(always)]
    #[must_use]
    fn from(value: &UintSlice) -> Self {
        UintVec(Vec::from(&value.0))
    }
}

impl From<Vec<Bigit>> for UintVec {
    #[inline(always)]
    #[must_use]
    fn from(value: Vec<Bigit>) -> Self {
        UintVec(value)
    }
}

impl Clone for UintVec {
    #[inline]
    #[must_use]
    fn clone(&self) -> Self {
        Self::from(Vec::from(&self.trim_zeros().0))
    }
}

impl Hash for UintVec {
    #[inline(always)]
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.deref().hash(state)
    }
}

impl Eq for UintVec {}

impl<U: Deref<Target = UintSlice>> PartialEq<U> for UintVec {
    #[inline(always)]
    #[must_use]
    fn eq(&self, other: &U) -> bool {
        self.deref().eq(&*other)
    }
}

impl Ord for UintVec {
    #[inline(always)]
    #[must_use]
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        self.deref().cmp(&*other)
    }
}

impl<U: Deref<Target = UintSlice>> PartialOrd<U> for UintVec {
    #[inline(always)]
    #[must_use]
    fn partial_cmp(&self, other: &U) -> Option<core::cmp::Ordering> {
        self.deref().partial_cmp(&*other)
    }
}

#[cfg(feature = "fmt")]
impl fmt::Binary for UintVec {
    #[inline(always)]
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

#[cfg(feature = "fmt")]
impl fmt::Octal for UintVec {
    #[inline(always)]
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

#[cfg(feature = "fmt")]
impl fmt::LowerHex for UintVec {
    #[inline(always)]
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

#[cfg(feature = "fmt")]
impl fmt::UpperHex for UintVec {
    #[inline(always)]
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

#[cfg(feature = "fmt")]
impl fmt::Display for UintVec {
    #[inline(always)]
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

#[cfg(feature = "fmt")]
impl fmt::LowerExp for UintVec {
    #[inline(always)]
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

#[cfg(feature = "fmt")]
impl fmt::UpperExp for UintVec {
    #[inline(always)]
    #[must_use]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.deref().fmt(f)
    }
}

// macro_rules! vec_index_impl { ($sourcetype:ty, $slicetype:ty, $ty:ty) => {
//     impl Index<$ty> for $sourcetype {
//         type Output = $slicetype;
//         #[inline(always)]
//         #[must_use]
//         fn index(&self, index: $ty) -> &Self::Output {
//             <&$slicetype>::from(self.0.index(index))
//         }
//     }

//     impl IndexMut<$ty> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn index_mut(&mut self, index: $ty) -> &mut Self::Output {
//             <&mut $slicetype>::from(self.0.index_mut(index))
//         }
//     }

//     impl Get<$ty> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn get(&self, index: $ty) -> Option<&Self::Output> {
//             match self.0.get(index) {
//                 Some(slice) => Some(<&$slicetype>::from(slice)),
//                 None => None,
//             }
//         }

//         #[inline(always)]
//         #[must_use]
//         unsafe fn get_unchecked(&self, index: $ty) -> &Self::Output {
//             // SAFETY: guaranteed by caller
//             <&$slicetype>::from(unsafe { self.0.get_unchecked(index) })
//         }
//     }

//     impl GetMut<$ty> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn get_mut(&mut self, index: $ty) -> Option<&mut Self::Output> {
//             match self.0.get_mut(index) {
//                 Some(slice) => Some(<&mut $slicetype>::from(slice)),
//                 None => None,
//             }
//         }

//         #[inline(always)]
//         #[must_use]
//         unsafe fn get_unchecked_mut(&mut self, index: $ty) -> &mut Self::Output {
//             // SAFETY: guaranteed by caller
//             <&mut $slicetype>::from(unsafe { self.0.get_unchecked_mut(index) })
//         }
//     }
// } }

macro_rules! vec_impl { ($sourcetype:ty, $slicetype:ty, $output:ty) => {
    impl Deref for $sourcetype {
        type Target = $slicetype;
        #[inline(always)]
        #[must_use]
        fn deref(&self) -> &Self::Target {
            self.0.as_slice().into()
        }
    }

    impl DerefMut for $sourcetype {
        #[inline(always)]
        #[must_use]
        fn deref_mut(&mut self) -> &mut Self::Target {
            self.0.as_mut_slice().into()
        }
    }

//     vec_index_impl!($sourcetype, $slicetype, (Bound<usize>, Bound<usize>));
//     vec_index_impl!($sourcetype, $slicetype, Range<usize>);
//     vec_index_impl!($sourcetype, $slicetype, RangeFull);
//     vec_index_impl!($sourcetype, $slicetype, RangeFrom<usize>);
//     vec_index_impl!($sourcetype, $slicetype, RangeInclusive<usize>);
//     vec_index_impl!($sourcetype, $slicetype, RangeTo<usize>);
//     vec_index_impl!($sourcetype, $slicetype, RangeToInclusive<usize>);

//     impl Index<usize> for $sourcetype {
//         type Output = $output;
//         #[inline(always)]
//         #[must_use]
//         fn index(&self, index: usize) -> &Self::Output {
//             self.0.index(index)
//         }
//     }

//     impl IndexMut<usize> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn index_mut(&mut self, index: usize) -> &mut Self::Output {
//             self.0.index_mut(index)
//         }
//     }

//     impl<'a> IntoIterator for &'a $sourcetype {
//         type Item = &'a $output;
//         type IntoIter = core::slice::Iter<'a, $output>;
//         #[inline(always)]
//         #[must_use]
//         fn into_iter(self) -> Self::IntoIter {
//             self.0.iter()
//         }
//     }

//     impl<'a> IntoIterator for &'a mut $sourcetype {
//         type Item = &'a mut $output;
//         type IntoIter = core::slice::IterMut<'a, $output>;
//         #[inline(always)]
//         #[must_use]
//         fn into_iter(self) -> Self::IntoIter {
//             self.0.iter_mut()
//         }
//     }

//     impl IntoIterator for $sourcetype {
//         type Item = $output;
//         type IntoIter = alloc::vec::IntoIter<$output>;
//         #[inline(always)]
//         #[must_use]
//         fn into_iter(self) -> Self::IntoIter {
//             self.0.into_iter()
//         }
//     }

//     impl Get<usize> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn get(&self, index: usize) -> Option<&Self::Output> {
//             self.0.get(index)
//         }

//         #[inline(always)]
//         #[must_use]
//         unsafe fn get_unchecked(&self, index: usize) -> &Self::Output {
//             // SAFETY: guaranteed by caller
//             unsafe { self.0.get_unchecked(index) }
//         }
//     }

//     impl GetMut<usize> for $sourcetype {
//         #[inline(always)]
//         #[must_use]
//         fn get_mut(&mut self, index: usize) -> Option<&mut Self::Output> {
//             self.0.get_mut(index)
//         }

//         #[inline(always)]
//         #[must_use]
//         unsafe fn get_unchecked_mut(&mut self, index: usize) -> &mut Self::Output {
//             // SAFETY: guaranteed by caller
//             unsafe { self.0.get_unchecked_mut(index) }
//         }
//     }
} }

vec_impl!(UintVec, UintSlice, Bigit);
vec_impl!(MaybeUninitUintVec, MaybeUninitUintSlice, MaybeUninit<Bigit>);
