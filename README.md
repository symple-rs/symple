# Symple

[![Test Status](https://gitlab.com/symple-rs/symple/badges/master/pipeline.svg)](https://gitlab.com/symple-rs/symple/-/pipelines)
[![Coverage](https://gitlab.com/symple-rs/symple/badges/master/coverage.svg)](https://gitlab.com/symple-rs/symple/-/graphs/master/charts)
[![Crate](https://img.shields.io/crates/v/symple.svg)](https://crates.io/crates/symple)
[![Book](https://img.shields.io/badge/book-master-green.svg)](https://symple-rs.gitlab.io/book)
[![API](https://img.shields.io/badge/api-master-green.svg)](https://symple-rs.gitlab.io/symple/symple)
[![API](https://docs.rs/symple/badge.svg)](https://docs.rs/symple)
[![License](https://img.shields.io/crates/l/symple.svg)](COPYRIGHT)

A high-performance pure-Rust math library

# Documentation

- [The Symple Book](https://symple-rs.gitlab.io/book)
- [API reference (main)](https://symple-rs.gitlab.io/symple/symple)
- [API reference (docs.rs)](https://docs.rs/symple)

# License

Symple is distributed under the terms of the MIT License or the Apache License (Version 2.0) at your option.

See [LICENSE-APACHE](LICENSE-APACHE), [LICENSE-MIT](LICENSE-MIT), and [COPYRIGHT](COPYRIGHT) for details.